﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Classwork0603
{
    class Program
    {
        static void Main()
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();
            dic[1] = "hello";
            foreach(var t in dic)
                Console.WriteLine(t.Value);
        }
    }
}
