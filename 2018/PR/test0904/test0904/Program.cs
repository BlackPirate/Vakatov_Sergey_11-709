﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace test0904
{
    class Program
    {
        static void Main()
        {
            {
                string[] items = { "123", "456", "7" };
                int k = 1;
                var result1 = items
                    .Take(k)
                    .SelectMany(t =>
                    {
                        int i = 0;
                        return t.Where(x => ++i % 2 == 1);
                    }).ToArray();
                var result2 = items
                    .Skip(k)
                    .SelectMany(t =>
                    {
                        int i = 0;
                        return t.Where(x => ++i % 2 == 0);
                    }).ToArray();
                var final = result1.Concat(result2).Reverse();
            }

            {
                var result = File.ReadAllLines("input.txt")
                    .Select(x =>
                    {
                        var data = x.Split(' ');
                        return new
                        {
                            School = int.Parse(data[0]),
                            Year = int.Parse(data[1]),
                            LastName = data[2]
                        };
                    })
                    .GroupBy(x => x.Year)
                    .Select(x => Tuple.Create(x.Select(t => t.School).Distinct().Count(), x.Key))
                    .OrderBy(x => x)
                    .Select(x => $"{x.Item1} {x.Item2}");
                File.WriteAllLines("output.txt", result);
            }
        }
    }

    static class EnExt
    {
        public static IEnumerable<TOut> DecMult<T1, T2, TOut>(this IEnumerable<T1> first, IEnumerable<T2> second, Func<T1, T2, TOut> selector)
        {
            var originalEn = first.GetEnumerator();
            while (originalEn.MoveNext())
                foreach (var item in second)
                    yield return selector(originalEn.Current, item);
        }
    }
}
