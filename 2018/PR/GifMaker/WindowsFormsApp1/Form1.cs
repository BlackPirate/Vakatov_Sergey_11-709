﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Forms;
using System.Net;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using System.IO;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private string request = "funny";
        private WebManagercs web = new WebManagercs();
        private string stringToSearch
        {
            get
            {
                return $"http://api.giphy.com/v1/gifs/search?q={ request.Split(' ').Aggregate((x, y) => x + "+" + y) }&api_key=ijbPRVtuGQxaNOjptXwx0Z5oor9DoOc0&limit=5";
            }
        }

        public Form1()
        {
            InitializeComponent();
        }

        private async Task<GifSearchResult> SearchGifAsync()
        {
            var result = await web.GetData(new Uri(stringToSearch));
            return JsonConvert.DeserializeObject<GifSearchResult>(result.ResultJson);
        }

        private async void DownloadGifAsync()
        {
            var result = await SearchGifAsync();
            int i = 0;
            foreach (var data in result.Data)
            {
                if (File.Exists($"download{i}.gif"))
                    File.Delete($"download{i}.gif");
                new WebClient().DownloadFileAsync(new Uri(data.Images.Downsized.Url), $"download{i++}.gif");
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            request = textBox1.Text;
            DownloadGifAsync();
            Thread thread = new Thread(UpdateImages);
            thread.Start();
        }

        private void UpdateImages()
        {
            Thread.Sleep(5000);
            pictureBox1.Image = (Image)(new Bitmap("download0.gif"));
            Thread.Sleep(3000);
            pictureBox2.Image = (Image)(new Bitmap("download1.gif"));
            Thread.Sleep(3000);
            pictureBox3.Image = (Image)(new Bitmap("download2.gif"));
            Thread.Sleep(3000);
            pictureBox7.Image = (Image)(new Bitmap("download3.gif"));
            Thread.Sleep(3000);
            pictureBox9.Image = (Image)(new Bitmap("download4.gif"));
        }
    }
}
