﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstTaskSem
{
    class Program
    {
        static HashSet<int> results = new HashSet<int>();
        static void Main(string[] args)
        {
            int lenght = 5;
            var t = GetOperatios(new char[lenght * 2 + 1], 0, 0);
        }

        static IEnumerable<char[]> GetOperatios(char[] current, int index, int result)
        {
            if (index == current.Length)
            {
                if (!results.Contains(result))
                    yield return current.ToArray();
                yield break;
            }
            if (index > 0 && (char.IsDigit(current[index - 1]) || current[index-1] == 'x'))
            {
                current[index] = '+';
                GetOperatios(current, index + 1, result);
                current[index] = '-';
                GetOperatios(current, index + 1, result);
                current[index] = '*';
                GetOperatios(current, index + 1, result);
            }

        }
    }
}
