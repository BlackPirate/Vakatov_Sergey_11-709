﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Reflection;

namespace ReflectionTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Assembly asm = Assembly.LoadFrom("WriteHello.exe");
            Type t = asm.GetType("PrintHello.Program", true, true);
            object obj = Activator.CreateInstance(t);
            MethodInfo method = t.GetMethod("PrintHello");
            method.Invoke(null, null);
        }
    }
}
