﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork3003
{
    public class Graph
    {
        public Node[] Vertex { get; set; }

        public class Node
        {
            public int Value { get; set; }
            public List<Node> Next { get; set; }
        }

        public static Graph Create(int[,] matrix)
        {
            var count = matrix.GetLength(0);
            Graph result = new Graph();
            result.Vertex = new Node[count];
            for (int i = 0; i < count; i++)
                for (int j = 0; j < count; j++)
                    if (matrix[i, j] != 0)
                    {
                        if (result.Vertex[i] == null)
                            result.Vertex[i] = new Node() { Value = 1, Next = new List<Node>() };
                        result.Vertex[i].Next.Add(result.Vertex[j]);
                        if (result.Vertex[j] == null)
                            result.Vertex[j] = new Node() { Value = 1, Next = new List<Node>() };
                        result.Vertex[j].Next.Add(result.Vertex[i]);
                    }

            return result;
        }
    }
}