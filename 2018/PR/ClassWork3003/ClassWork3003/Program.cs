﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork3003
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] myGraph = new[,]
            {
                {0, 1, 1, 0, 0},
                {1, 0, 0, 1, 0},
                {1, 0, 0, 1, 0},
                {0, 1, 1, 0, 1},
                {0, 0, 0, 1, 0}
            };

            int[][] myNextGraph = new int[5][];
            for (int i = 0; i < myNextGraph.Length; i++)
            {
                myNextGraph[i] = new int[5];
                for (int j = 0; j < myNextGraph.Length; j++)
                    myNextGraph[i][j] = myGraph[i, j];
            }

            Graph graph = Graph.Create(myGraph);

            bool[] visited = new bool[myNextGraph.Length];
            for (int i = 0; i < myNextGraph.Length; i++)
                Visit(visited, myNextGraph, i);
        }

        public static int EdgesCount(int[,] graph, int vertex)
        {
            var sum = 0;
            int count = graph.GetLength(0);
            for (int i = 0; i < count; i++)
                sum += graph[vertex, i];
            return sum;
        }

        public static int EdgesCount(int[][] graph, int vertex)
            => graph[vertex].Sum(x => x == 0 ? 0 : 1);

        public static int[] GetVertex(int[][] graph, int vertex)
        {
            var result = new int[graph.Length];
            for (int i = 0; i < graph.Length; i++)
                if (graph[vertex][i] != 0)
                    result[i] = i;
            return result.Where(x => x != 0).ToArray();
        }

        public static int[] GetVertex(int[,] graph, int vertex)
        {
            int count = graph.GetLength(0);
            var result = new int[count];
            for (int i = 0; i < count; i++)
                if (graph[vertex, i] != 0)
                    result[i] = i;
            return result.Where(x => x != 0).ToArray();
        }

        public static void Visit(bool[] visited, int[][] graph, int vertex)
        {
            if (visited[vertex])
                return;
            visited[vertex] = true;
            Console.WriteLine(vertex);

            for (int i = 0; i < graph[vertex].Length; i++)
                if (graph[vertex][i] != 0)
                    Visit(visited, graph, i);
        }
    }
}
