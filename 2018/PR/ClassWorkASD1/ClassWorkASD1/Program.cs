﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWorkASD1
{
    class Program
    {
        static void Main()
        {
            int n = 10;
            int[] original = new int[n];
            int[] tempArray = new int[int.MaxValue];

            for (int i = 0; i < original.Length; i++)
                tempArray[original[i]]++;

            for (int i = 0; i < tempArray.Length; i++)
                if (tempArray[i] > 0)
                    Console.WriteLine($"{i} : {tempArray[i]} раз");
        }

        static void Main2()
        {
            int n = 10;
            int[] original = new int[n];
            Dictionary<int, int> result = new Dictionary<int, int>();

            foreach (var t in original)
            {
                if (!result.ContainsKey(t))
                    result.Add(t, 0);
                result[t]++;
            }
        }
    }
}
