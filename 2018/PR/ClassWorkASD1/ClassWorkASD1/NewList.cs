﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWorkASD1
{
    class NewList<T>
    {
        public class Node
        {
            public T Value { get; set; }
            public Node Next;
        }

        public Node Root { get; set; }
        public int Count { get; set; }
        public Node Tail { get; set; }
        public bool IsEmpty { get { return Count == 0; } }

        public void Add(T value)
        {
            throw new NotImplementedException();
        }

        public bool Remove(T value)
        {
            throw new NotImplementedException();
        }

        public NewList<T> Invert()
        {
            var current = Root;
            Node[] tempArray = new Node[Count];

            for (int i = 0; i < Count; i++)
            {
                tempArray[i] = current;
                current = current.Next;
            }

            for (int i = Count - 1; i > 0; i--)
                tempArray[i].Next = tempArray[i - 1];

            NewList<T> result = new NewList<T>
            {
                Count = this.Count,
                Root = tempArray[Count - 1],
                Tail = tempArray[0]
            };

            return result;
        }
    }
}
