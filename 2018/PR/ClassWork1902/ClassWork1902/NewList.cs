﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWorkASD1
{
    class NewList<T> : IEnumerable<T>, ICollection<T>
    {
        public class Node
        {
            public T Value { get; set; }
            public Node Next;
        }

        public Node Root { get; set; }
        public int Count { get; set; }
        public Node Tail { get; set; }
        public bool IsEmpty { get { return Count == 0; } }
        public bool IsReadOnly => isReadOnly;

        private bool isReadOnly { get; set; }

        public NewList(bool isReadOnly = false)
        {
            this.isReadOnly = isReadOnly;
        }

        public void Add(T value)
        {
            var temp = new Node() { Value = value };
            if (IsEmpty)
                Root = Tail = temp;
            else
            {
                Tail.Next = temp;
                Tail = temp;
            }
        }

        public bool Remove(T value)
        {
            var previous = Root;
            var current = Root;
            bool exist = false;
            for (int i = 0; i < Count; i++)
            {
                if(current.Value.Equals(value))
                {
                    previous.Next = current.Next;
                    Count--;
                    exist = true;
                    current = null;
                }
                if (current != null)
                    previous = current;
                current = previous.Next;
            }
            return exist;
        }

        public NewList<T> Invert()
        {
            var current = Root;
            Node[] tempArray = new Node[Count];

            for (int i = 0; i < Count; i++)
            {
                tempArray[i] = current;
                current = current.Next;
            }

            for (int i = Count - 1; i > 0; i--)
                tempArray[i].Next = tempArray[i - 1];

            NewList<T> result = new NewList<T>
            {
                Count = this.Count,
                Root = tempArray[Count - 1],
                Tail = tempArray[0]
            };

            return result;
        }

        public IEnumerator GetEnumerator()
        {
            var current = Root;
            while (current != null)
            {
                yield return current;
                current = current.Next;
            }
        }

        public void Clear()
        {
            this.Count = 0;
            this.Root = null;
            this.Tail = null;
        }

        public bool Contains(T item)
        {
            var current = Root;
            for (int i = 0; i < Count; i++)
            {
                if (current.Value.Equals(item))
                    return true;
                current = current.Next;
            }
            return false;
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            var current = Root;
            for (int i = 0; i < Count; i++)
            {
                if (i >= arrayIndex)
                    array[i] = current.Value;
                current = current.Next;
            }
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            var current = Root;
            while (current != null)
            {
                yield return current.Value;
                current = current.Next;
            }
        }
    }
}
