﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace classwork2703
{
    static class EnumerableExt
    {
        public static void Show<T>(this IEnumerable<T> items)
        {
            foreach(var t in items)
                Console.Write("{0} ", t);
            Console.WriteLine();
        }

        public static string GetString(this IEnumerable<string> items)
        {
            return items.Aggregate((x, y) => x.Substring(0, 1) + y.Substring(0, 1));
        }

        public static IEnumerable<string> GetStringFromInt(this IEnumerable<int> items)
        {
            return items.Where(x => x % 2 == 1).Select(x => x.ToString()).OrderBy(x => x);
        }
    }
}
