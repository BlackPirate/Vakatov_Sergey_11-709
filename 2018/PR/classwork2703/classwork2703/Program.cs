﻿using System.IO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace classwork2703
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1
            {
                string fileName = "input.txt";
                var result = File.ReadAllLines(fileName)
                    .Select(x =>
                    {
                        var res = x.Split(' ');
                        return new
                        {
                            LastName = res[0],
                            Year = int.Parse(res[1]),
                            School = int.Parse(res[2])
                        };
                    })
                    .GroupBy(x => x.School)
                    .Select(x => $"{x.Key} {x.Count()} {x.First().LastName}").ToArray();
            }

            // 2
            {
                string fileName = "";
                var result = File.ReadAllLines(fileName)
                    .Select(x =>
                   {
                       var res = x.Split(' ');
                       return new
                       {
                           Company = res[0],
                           Number = int.Parse(res[1]),
                           Price = int.Parse(res[2]),
                           Street = res[3]
                       };
                   })
                    .GroupBy(x => x.Number)
                    .OrderBy(x => x.Key)
                    .Select(x => $"{x.Key} min: {x.Min(t => t.Price)} max: {x.Max(t => t.Price)}");
            }

            // 3
            {
                string fileName = "";
                var result = File.ReadAllLines(fileName)
                    .Select(x =>
                    {
                        var res = x.Split(' ');
                        return new
                        {
                            School = int.Parse(res[0]),
                            LastName = res[1],
                            Name = res[2],
                            MathScore = int.Parse(res[3]),
                            Russian = int.Parse(res[4]),
                            ComputerScience = int.Parse(res[5]),
                        };
                    })
                    .GroupBy(x => x.School)
                    .Select(x => x.OrderBy(t => t.Russian + t.MathScore + t.ComputerScience)
                                .ThenBy(t => t.LastName)
                                .ThenBy(t => t.Name).First())
                    .OrderBy(x => x.School)
                    .Select(x => $"{x.School} {x.Russian + x.MathScore + x.ComputerScience} {x.LastName} {x.Name}");
            }
        }
    }
}
