﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork1
{
    class NewList<T>
    {
        public Node<T> Root;
        public int Count { get; private set; }
        public Node<T> Tail;

        public bool IsEmpty { get { return Count == 0; } }

        public NewList()
        {
            Count = 0;
        }

        public void Add(T data)
        {
            var tempNode = new Node<T> { Value = data };
            if (IsEmpty)
            {
                Root = tempNode;
                Tail = Root;
            }
            else
            {
                Tail.Next = tempNode;
                Tail = tempNode;
            }
            Count++;
        }

        public T GetValue(int index)
        {
            var current = Root;
            for (int i = 0; i < index; i++)
                current = current.Next;
            return current.Value;
        }

        public void Insert(T data, int index)
        {
            if (index == 0)
                InsertAtZero(data);
            var currentNode = Root;
            var preview = Root;
            for (int i = 0; i < index; i++)
            {
                preview = currentNode;
                currentNode = currentNode.Next;
            }
            var tempNode = new Node<T> { Value = data, Next = currentNode };
            preview.Next = tempNode;
        }

        private void InsertAtZero(T data)
        {
            var temp = new Node<T> { Value = Root.Value, Next = Root.Next };
            Root = new Node<T> { Value = data, Next = temp };
        }

        public void Remove(T data)
        {

        }

        public void Clear()
        {
            this.Count = 0;
        }

    }

    class Node<T>
    {
        public T Value { get; set; }
        public Node<T> Next { get; set; }
    }
}
