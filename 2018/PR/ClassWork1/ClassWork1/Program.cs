﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork1
{
    class ProNode<T>
    {
        public ProNode<T> Preview;
        public ProNode<T> Next;
        public IComparable Value;

        public void Add(IComparable value)
        {
            if (value.CompareTo(this.Value) < 0 && value.CompareTo(this.Preview?.Value) < 0)
            {
                this.Preview.Add(value);
            }
            else if (value.CompareTo(this.Value) > 0 && value.CompareTo(this.Next?.Value) > 0)
            {
                this.Next.Add(value);
            }
            else if (value.CompareTo(this.Value) < 0)
            {
                AddPreview(value);
            }
            else
            {
                AddNext(value);
            }
        }

        private void AddPreview(IComparable value)
        {
            ProNode<T> temp = new ProNode<T>();
                temp.Next = this;
                temp.Preview = this.Preview;
                temp.Value = value;
                this.Preview = temp;
        }

        private void AddNext(IComparable value)
        {
            ProNode<T> temp = new ProNode<T>();
            temp.Preview = this;
            temp.Next = this.Next;
            temp.Value = value;
            this.Next = temp;
        }
    }
}
