﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork1
{
    class Node1 <T> : IComparable
    {
        public IComparable Value { get; set; }
        public Node<T> Next { get; set; }

        public void BubbleSort(Node<T> root)
        {
            var tempCurrent = root;
            while (tempCurrent.Next != null)
            {
                var current = root;
                while (current.Next != null)
                {
                    if (current.CompareTo(current.Next) > 0)
                    {
                        var temp = current;
                        var next = current.Next.Next;
                        current = current.Next;
                        current.Next = temp;
                        current.Next.Next = next;
                    }
                    current = current.Next;
                }
                tempCurrent = current.Next;
            }
        }

        public int CompareTo(object obj)
        {
            var temp = (Node<T>)obj;
            return this.Value.CompareTo(temp.Value);
        }
    }
}
