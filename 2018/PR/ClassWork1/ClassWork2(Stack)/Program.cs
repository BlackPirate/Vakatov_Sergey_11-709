﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork2_Stack_
{
    class CustomStack
    {
        private int[] items;
        private int count;

        public bool IsEmpty { get { return count == 0; } }

        public CustomStack(int lenght)
        {
            items = new int[lenght];
        }

        public void Push(int data)
        {
            if (count >= items.Length)
                Resize();
            items[count] = data;
            count++;
        }

        public int Pop()
        {
            if(count != 0)
            {
                var t = items[count - 1];
                count--;
                return t;
            }
            return -1;
        }

        public int Peek()
        {
            if (count > 0)
                return items[count - 1];
            else return -1;
        }

        public void Resize(int sizeToAdd = 25)
        {
            var tempArray = new int[items.Length + sizeToAdd];
            for (int i = 0; i < items.Length; i++)
                tempArray[i] = items[i];
            items = tempArray;
        }
    }
}
