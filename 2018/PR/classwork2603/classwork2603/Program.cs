﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace classwork2603
{
    class Program
    {
        static void Main()
        {
            int[] a = { 1, 1, 2, 3, 4, 24, 1, 1, 1 }, b = a;
            string[] s = { "", "aa", "22" }, d = s;
            // 1
            {
                int k1 = 1, k2 = 3;
                var result = a
                    .Where(x => x > k1)
                    .Concat(b.Where(x => x < k2))
                    .OrderBy(x => x);
            }

            // 2
            {
                int l1 = 2, l2 = 3;
                var res = s
                    .Where(x => x.Length == l1)
                    .Concat(d.Where(x => x.Length == l2))
                    .OrderByDescending(x => x);
            }

            // 3
            {
                var res = a
                    .Select(e =>
                    {
                        var sum = b
                            .Where(t => t % 10 == e % 10)
                            .DefaultIfEmpty(0)
                            .Sum();
                        return Tuple.Create(sum, e);
                    })
                    .OrderBy(x => x.Item1)
                    .ThenByDescending(x => x.Item2)
                    .Select(x => $"{x.Item1}:{x.Item2}");
            }

            // 4
            {
                var res = a
                    .Select(t => Tuple.Create(t, a.Where(x => x % 10 == t % 10).Sum()))
                    .OrderBy(x => x.Item1)
                    .Select(x => $"{x.Item1}:{x.Item2}");
            }

            // 
        }
    }
}
