﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace classwork2602
{
    class Program
    {
        static void Main()
        {
            HashSet<string> firstStrings = new HashSet<string>();
            HashSet<string> secondStrings = new HashSet<string>();

            var tempArray1 = System.IO.File.ReadAllLines("file1.txt");
            var tempArray2 = System.IO.File.ReadAllLines("file2.txt");

            foreach (var t in tempArray1)
                firstStrings.Add(t);
            foreach (var t in tempArray2)
                secondStrings.Add(t);

            var result = firstStrings.Union(secondStrings);
        }
    }
}
