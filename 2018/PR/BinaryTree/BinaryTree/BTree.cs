﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree
{
    public class BTree<T> where T : IComparable
    {
        public Node Root { get; set; }

        public class Node
        {
            public T Value { get; set; }
            public Node Right { get; set; }
            public Node Left { get; set; }
        }

        public void AddRandom(T[] original, Node current = null, int index = 0, Node previous = null)
        {
            if (index != original.Length)
            {
                var nodeToAdd = new Node { Value = original[index] };
                if (Root == null)
                {
                    Root = nodeToAdd;
                    AddRandom(original, Root, index + 1, Root);
                }
                else if (current.Left == null)
                {
                    current.Left = nodeToAdd;
                    AddRandom(original, current, index + 1, previous);
                }
                else if (current.Right == null)
                {
                    current.Right = nodeToAdd;
                    AddRandom(original, current, index + 1, previous);
                }
                else if (previous.Left == null)
                    AddRandom(original, previous.Left, index + 1, previous);
                else if (previous.Right == null)
                    AddRandom(original, previous.Right, index + 1, previous);
                else AddRandom(original, current.Left, index + 1, current);
            }
        }

        public void AddSorted(T[] original, int start, int end, Node current)
        {
            if (start != end)
            {
                int middle = (start + end) / 2;
                var node = new Node { Value = original[middle] };
                if (Root == null)
                {
                    Root = node;
                    current = Root;
                }
                else if (current.Value.CompareTo(node.Value) < 0)
                {
                    current.Right = node;
                    current = current.Right;
                }
                else
                {
                    current.Left = node;
                    current = current.Left;
                }
                AddSorted(original, start, middle, current);
                AddSorted(original, middle + 1, end, current);
            }
        }

    }
}
