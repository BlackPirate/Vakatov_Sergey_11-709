﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree
{
    class Program
    {
        static void Main()
        {
            BTree<int> tree = new BTree<int>();
            //tree.AddRandom(new[] { 1, 2, 4, 6, 1, 3, 4, 4 });
            tree.AddSorted(new[] { 1, 2, 3, 4, 5, 6, 7, 8 }, 0, 8, null);
        }
    }
}
