﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigTask1
{
    public class CycleAtGraph
    {
        public static bool GetPath()
        {
            var graph = new Graph(5);
            graph.Connect(0, 1);
            graph.Connect(1, 2);
            graph.Connect(2, 3);
            graph.Connect(3, 4);

            Stack<Node> stack = new Stack<Node>();
            HashSet<Edge> visited = new HashSet<Edge>();
            List<Node> path = new List<Node>();

            var vertex = graph.Nodes.Where(x => x.IncidentEdges.Count() % 2 == 1);
            if (vertex.Count() != 2)
                return false;

            stack.Push(vertex.First());
            while (stack.Count != 0)
            {
                var v = stack.Peek();
                foreach (var e in v.IncidentEdges)
                {
                    if (!visited.Contains(e))
                    {
                        stack.Push(e.OtherNode(v));
                        visited.Add(e);
                        break;
                    }
                }
                if (v == stack.Peek())
                {
                    stack.Pop();
                    path.Add(v);
                }
            }
            Console.WriteLine(path);
            return true;
        }
    }
}
