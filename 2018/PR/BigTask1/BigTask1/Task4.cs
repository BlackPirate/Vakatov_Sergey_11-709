﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigTask1
{
    class Task4
    {
        static double GetPrice(Dictionary<double, double> items, int maxMass)
        {
            double finalSum = 0;
            double currentMass = 0;
            var gold = items
                .Select(t => new
                {
                    PriceByKg = t.Key / t.Value,
                    Price = t.Key,
                    Mass = t.Value
                })
                .OrderByDescending(x => x.PriceByKg).ToList();
            for(int i = 0; i < gold.Count; i++)
            {
                currentMass += gold[i].Mass;
                finalSum += gold[i].Price;
                if (currentMass >= maxMass)
                {
                    finalSum -= (currentMass - maxMass) * gold[i].PriceByKg;
                    currentMass = maxMass;
                    break;
                }
            }
            return finalSum;
        }
    }
}
