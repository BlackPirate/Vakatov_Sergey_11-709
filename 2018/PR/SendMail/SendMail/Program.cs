﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Net;

namespace SendMail
{
    class Program
    {
        static void Main()
        {
            for (int i = 1; i <= 10; i++)
            {
                Console.Beep(370*i, 100);
            }
            var userData = Authorization();
            var login = userData.Item1;
            var password = userData.Item2;
            var oftherEmail = Console.ReadLine();
            var sitesData = GetDataAboutSites();
            var theme = "Info about sites";

            var body = sitesData
                    .Select(x =>
                    {
                        var data = x.Split(' ');
                        return $"<tr><td>{data[0]}</td><td>{data[1]}</td><tr>";
                    }).Aggregate((x, y) => $"{x} \n {y}");
            body = "<table>" + body + "</table>";

            SendMail(login, password, theme, body, oftherEmail);
        }

        static Tuple<string, string> Authorization()
        {
            throw new NotImplementedException();
        }

        static List<string> GetDataAboutSites()
        {
            throw new NotImplementedException();
        }

        static void SendMail(string login, string password, string theme, string body, string oftherEmail)
        {
            SmtpClient Smtp = new SmtpClient("smtp.yandex.ru", 587);
            Smtp.Credentials = new NetworkCredential(login, password);
            MailMessage Message = new MailMessage();
            Message.From = new MailAddress(login);
            Message.To.Add(new MailAddress(oftherEmail));
            Message.Subject = theme;
            Message.Body = body;

            try
            {
                Smtp.Send(Message);
            }
            catch (SmtpException)
            {
                Console.WriteLine("Ошибка!");
            }
        }
    }
}
