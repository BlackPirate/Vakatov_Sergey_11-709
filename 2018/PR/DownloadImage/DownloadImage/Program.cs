﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Xml;
using System.Text.RegularExpressions;

namespace DownloadImage
{
    class Program
    {
        static void Main(string[] args)
        {
            WebClient client = new WebClient();
            var html = client.DownloadString(@"https://www.dreamstime.com/free-images_pg1");

            //XmlDocument doc = new XmlDocument();
            //doc.Load(@"https://www.dreamstime.com/free-images_pg1");
            ////doc.LoadXml("index.html");
            //XmlNodeList elemList = doc.GetElementsByTagName("img");
            //for (int i = 0; i < elemList.Count; i++)
            //{
            //    Console.WriteLine(elemList[i].InnerXml);
            //}

            Regex reHref = new Regex(@"(?inx)
            <img \s [^>]*
            src \s* = \s*
            (?<q> ['""] )
                (?<url> [^""]+ )
            \k<q>
            [^>]* >");
            int i = 0;
            foreach (Match match in reHref.Matches(html))
                DownloadImageByURL(match.Groups["url"].ToString(), $"{i++}.png");
        }

        static void DownloadImageByURL(string url, string filename)
        {
            WebClient client = new WebClient();
            try
            {
                client.DownloadFile(url, filename);
            }
            catch (Exception)
            {
                Console.WriteLine("неверный адрес");
            }
        }
    }
}
