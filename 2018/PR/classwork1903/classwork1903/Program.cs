﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace classwork1903
{
    class Program
    {
        static void Main()
        {
            var data = File.ReadAllLines("students.txt");
            List<Student> students = new List<Student>();
            foreach (var t in data)
                students.Add(new Student(t));

            var listByGroup = students
                .OrderBy(x => x.GroupNum)
                .ThenBy(x => x.Name);
            //PrintStudentsByGroup(listByGroup);

            var list = listByGroup.Select(x => new { Num = x.GroupNum, Name = $"{x.Name[0]}. {x.LastName}" });
            foreach (var t in list)
                Console.WriteLine("{0} {1}", t.Num, t.Name);

            var tempScore = listByGroup.GroupBy(x => x.GroupNum);
            foreach (var t in tempScore)
                Console.WriteLine(t.Average(x => x.AvgScore));
        }

        static void PrintStudentsByGroup(IEnumerable<Student> listByGroup)
        {
            int prevGroup = 0;
            foreach (var t in listByGroup)
            {
                if (prevGroup != t.GroupNum)
                {
                    prevGroup = t.GroupNum;
                    Console.WriteLine(prevGroup);
                }
                Console.WriteLine($"{t.Name[0]}. {t.LastName}");
            }
        }
    }

    class Student
    {
        public double AvgScore { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public int GroupNum { get; set; }

        public Student(string data)
        {
            var tempDate = data.Split(' ');
            Name = tempDate[0];
            LastName = tempDate[1];
            GroupNum = int.Parse(tempDate[2]);
            AvgScore = int.Parse(tempDate[3]);
        }
    }
}
