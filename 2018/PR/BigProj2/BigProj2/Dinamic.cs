﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BigProj2
{
    class Dinamic
    {
        public static int BaggageTask(Tuple<int, int>[] items, int currentSum, int maxWieght, int index)
        {
            if (index == items.Length)
                return currentSum;
            int firstPath = 0, secondPath = 0;
            if (maxWieght - items[index].Item2 >= 0)
                firstPath = BaggageTask(items, currentSum + items[index].Item1, maxWieght - items[index].Item2, index + 1);
            secondPath = BaggageTask(items, currentSum, maxWieght, index + 1);
            return Math.Max(firstPath, secondPath);
        }

        public static int MaxSumTask(int[] array)
        {
            int result = array[0], currentSum = 0;
            for (int i = 0; i < array.Length; i++)
            {
                currentSum += array[i];
                result = Math.Max(result, currentSum);
                currentSum = Math.Max(currentSum, 0);
            }
            return result;
        }
    }
}
