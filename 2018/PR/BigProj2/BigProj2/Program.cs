﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net;
using System.Net.Mail;

namespace BigProj2
{
    class Program
    {
        static void Main()
        {
            var items = new[]
            {
                Tuple.Create(1,1),
                Tuple.Create(3,2),
                Tuple.Create(3,3),
                Tuple.Create(0, 1)
            };
            Console.WriteLine(Dinamic.BaggageTask(items, 0, 3, 0));
            int[] arr = new[] { 1, -1, 2, 3, -3, 0, 2, 4, -4, 5 };
            Console.WriteLine(Dinamic.MaxSumTask(arr));
        }

        static double BoyTask(double[] stairs, double currentSum = 0, int index = -1)
        {
            if (index != -1)
                currentSum += stairs[index];
            if (index + 2 >= stairs.Length)
                return currentSum;
            double next = BoyTask(stairs, currentSum, ++index);
            double nextNext = BoyTask(stairs, currentSum, ++index);
            return Math.Min(next, nextNext);
        }

        public static int MultiplyMatrix(int[] matrixSize)
        {
            int len = matrixSize.Length - 1;
            int[,] countOperations = new int[len + 1, len + 1];
            for (int i = 1; i <= len; i++)
                countOperations[i, i] = 0;

            for (int l = 2; l <= len; l++)
            {
                for (int i = 1; i <= len - l + 1; i++)
                {
                    int j = i + l - 1;
                    countOperations[i, j] = int.MaxValue;
                    for (int k = i; k <= j - 1; k++)
                    {
                        var path2 = countOperations[i, k] + countOperations[k + 1, j] + matrixSize[i - 1] * matrixSize[k] * matrixSize[j];
                        countOperations[i, j] = Math.Min(countOperations[i, j], path2);
                    }
                }
            }
            return countOperations[1, len];
        }

    }
}
