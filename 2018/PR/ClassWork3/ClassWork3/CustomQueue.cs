﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork3
{
    class CustomQueue<T>
    {
        public bool IsEmpty { get { return Count == 0; } }
        public int Count { get; set; }
        public Node Head { get; set; }
        public Node Tail { get; set; }

        public class Node
        {
            public Node Next { get; set; }
            public T Value { get; set; }
        }

        public T First()
        {
            if (IsEmpty)
                return default(T);
            return Head.Value;
        }

        public T Last()
        {
            if (IsEmpty)
                return default(T);
            return Tail.Value;
        }

        public void Enqueue(T value)
        {
            var tempNode = new Node { Value = value };
            if (IsEmpty)
                Head = tempNode;
            else
                Tail.Next = tempNode;
            Tail = tempNode;
            Count++;
        }

        public T Dequeue()
        {
            if (IsEmpty)
                throw new Exception("is empty");
            var tempValue = Head.Value;
            if (Count > 1)
                Head = Head.Next;
            else Head = Tail = null;
            Count--;
            return tempValue;
        }
    }
}
