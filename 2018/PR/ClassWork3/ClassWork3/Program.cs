﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassWork3
{
    class CustomStack<T>
    {
        public class Node
        {
            public Node Next { get; set; }
            public Node Preview { get; set; }
            public T Value { get; set; }
        }

        public Node Head { get; set; }
        public Node Tail { get; set; }
        public int Count { get; set; }
        public bool IsEmpty { get { return Count == 0; } }

        public void Push(T value)
        {
            if (IsEmpty)
            {
                Head = new Node { Value = value };
                Tail = Head;
            }
            else
            {
                var temp = new Node { Value = value };
                temp.Preview = Tail;
                Tail.Next = temp;
                Tail = temp;
            }
            Count++;
        }

        public T Pop()
        {
            if (IsEmpty)
                throw new Exception("Stack is empty");
            var tempValue = Tail.Value;
            if (Count > 1)
            {
                Tail.Preview.Next = null;
                Tail = Tail.Preview;
            }
            else
            {
                Head = Tail = null;
            }
            Count--;
            return tempValue;
        }

        public T Peek()
        {
            if (IsEmpty)
                throw new Exception("Stack is empty");
            return Tail.Value;
        }
    }
}
