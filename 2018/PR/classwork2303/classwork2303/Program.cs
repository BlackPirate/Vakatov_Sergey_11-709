﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace classwork2303
{
    class Program
    {
        static void Main()
        {
            List<int> list = new List<int> { 1, 100, 2, 4, 24, 56 };
            List<string> strings = new List<string>() { "aa", "", "sssss" };

            // First
            // 1
            double mult = list.Aggregate((x, y) => (x % 10) * (y % 10));

            // 2
            int first = list.Min(x =>
            {
                if (x > 0)
                    return x;
                return 0;
            });

            // 3
            {
                int d = 3;
                var firstModD = list.FirstOrDefault(x => x > 0 && x % 10 == d);
            }

            // Where
            // 4
            var newEnumerable = list.Where(x => x % 2 != 0).Skip(1).Distinct();

            // 5
            {
                var result = list.Where(x => x / 10 > 0 && x / 100 == 0).OrderBy(x => x); 
            }
            // 6
            {
                int d = 3, k = 3;
                var resultUnion = list.TakeWhile(x => x < d).Union(list.Skip(k - 1)).Distinct().OrderByDescending(x => x);
            }

            // 7
            {
                int k = 3;
                var result = list.Where(x => x % 2 == 0).Except(list.Take(k)).Distinct().Reverse();
            }

            // Select
            // 8
            {
                var result = strings.Select(x => x[0]).Reverse();
            }

            // 9
            {
                int i = 0, j = 0;
                var result = list
                    .Where(x => i++ % 3 == 0 ? false : true)
                    .Select(x => j++ % 2 == 0 ? x * 2 : x);
            }
        }
    }
}
