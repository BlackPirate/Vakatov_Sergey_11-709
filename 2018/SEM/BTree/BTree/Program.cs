﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BTree
{
    class Program
    {
        static void Main()
        {
            BTree<int, int> tree = new BTree<int, int>(2);
            for (int i = 0; i < 20; i++)
                tree.Insert(i, i);
            tree.Insert(25, 0);
            var element = tree.Search(1);
            tree.Delete(2);
        }
    }
}
