﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinkedList2;

namespace RunMyLinkedList2
{
    class Program
    {
        static int[] Original = new[] { 1, 5, 8, 3, 5, 6, 6, 2, 3 };
        static void Main(string[] args)
        {
            MyLinkedList2 list = new MyLinkedList2(Original);
            Console.WriteLine($"{list} - оригинальный лист");
            ShowInsert(list, 4);
            ShowDelete(list, 3);
            ShowDivide(list);
            ShowMaxValue(list);
            ShowMerge(list, new MyLinkedList2(new[] { 4, 1, 6, 3, 6, 3 }));
            ShowNewList(list);
        }

        static void ShowInsert(MyLinkedList2 list, int element)
        {
            Console.WriteLine($"\nвставляем {element} в лист: ({list})");
            list.Insert(element);
            Console.WriteLine($"результат: {list}");
        }

        static void ShowDelete(MyLinkedList2 list, int element)
        {
            Console.WriteLine($"\nудаляем {element} из ({list})");
            list.Delete(element);
            Console.WriteLine($"результат: {list}");
        }

        static void ShowDivide(MyLinkedList2 list)
        {
            Console.WriteLine($"\nразбиваем {list} на два: в первом кратные 3,а во втором некратные");
            var result = list.Divide();
            Console.WriteLine($"первый: {result[0]}");
            Console.WriteLine($"второй: {result[1]}");
        }

        static void ShowMaxValue(MyLinkedList2 list)
        {
            Console.WriteLine("\nмаксимальное число повторений какого-либо эелемента" +
                $" в {list} равно {list.MaxValue()}");
        }

        static void ShowMerge(MyLinkedList2 list, MyLinkedList2 secondLitToMerge)
        {
            Console.WriteLine($"\nСлияние списка {list} с {secondLitToMerge}:");
            Console.WriteLine(list.Merge(secondLitToMerge));
        }

        static void ShowNewList(MyLinkedList2 list)
        {
            Console.WriteLine("\nсоздаем новый список по правилу a[i]*a[count-i]:");
            Console.WriteLine(list + "- оригинальный список");
            Console.WriteLine(list.NewList() + "результат");
        }
    }
}
