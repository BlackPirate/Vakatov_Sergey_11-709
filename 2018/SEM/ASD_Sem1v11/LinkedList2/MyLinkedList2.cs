﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinkedList2
{
    public class MyLinkedList2 : IEnumerable
    {
        public class Node
        {
            public int Value { get; set; }
            public Node Next { get; set; }
            public Node Prev { get; set; }
        }

        public Node Root { get; set; }
        public Node Tail { get; set; }
        public int Count { get; set; }

        public MyLinkedList2(int[] original)
        {
            foreach (var item in original)
                Insert(item);
        }

        public void Insert(int value)
        {
            if (Root == null || Root.Value >= value)
                InsertRoot(value);
            else if (Tail.Value <= value)
                InsertTail(value);
            else
            {
                var current = Root;
                while (current != null && current.Value < value)
                    current = current.Next;
                var node = new Node { Value = value, Next = current, Prev = current.Prev };
                current.Prev = node;
                node.Prev.Next = node;
            }
            Count++;
        }

        private void InsertTail(int value)
        {
            var node = new Node { Value = value, Prev = Tail };
            Tail.Next = node;
            Tail = node;
        }

        private void InsertRoot(int value)
        {
            var node = new Node { Value = value, Next = Root };
            if (Root == null)
                Root = Tail = node;
            else
            {
                Root.Prev = node;
                Root = node;
            }
        }

        public void Delete(int value)
        {
            var current = Root;
            while (current != null)
            {
                if (current.Value == value)
                {
                    if (current == Root)
                    {
                        Root.Next.Prev = null;
                        Root = Root.Next;
                    }
                    else if (current == Tail)
                    {
                        Tail.Prev.Next = null;
                        Tail = Tail.Prev;
                    }
                    else
                    {
                        var next = current.Next;
                        current.Next.Prev = current.Prev;
                        current.Prev.Next = next;
                    }
                    Count--;
                }
                current = current.Next;
            }
        }

        public int MaxValue()
        {
            int maxCount = 1, currentCount = 1;
            var current = Root;
            while (current != null)
            {
                if (current.Value == current.Prev?.Value)
                    currentCount++;
                else
                {
                    if (currentCount > maxCount)
                        maxCount = currentCount;
                    currentCount = 1;
                }
                current = current.Next;
            }
            return Math.Max(maxCount, currentCount);
        }

        public MyLinkedList2 Merge(MyLinkedList2 listToMerge)
        {
            MyLinkedList2 result = null;
            var firstEnumerator = this.GetEnumerator();
            firstEnumerator.MoveNext();
            var secondEnumerator = listToMerge.GetEnumerator();
            secondEnumerator.MoveNext();
            int count = this.Count + listToMerge.Count;
            int min;
            int index1 = 1, index2 = 1;
            for (int i = 0; i < count; i++)
            {
                min = GetMinValue(firstEnumerator, secondEnumerator, 
                    ref index1, ref index2, listToMerge.Count);
                if (result == null)
                    result = new MyLinkedList2(new[] { min });
                else result.Insert(min);
                // В Insert() есть проверка, может ли текущий элемент встать последним,
                // поэтому пробегаться по всему списку не будет каждый раз.
            }
            return result ?? new MyLinkedList2(new int[0]);
        }

        private int GetMinValue(IEnumerator first, IEnumerator second, 
            ref int index1, ref int index2, int listCount)
        {
            int min;
            if (index2 > listCount|| (index1 <= this.Count
                    && (int?)first.Current < (int?)second.Current))
            {
                min = first.Current as int? ?? int.MaxValue;
                first.MoveNext();
                index1++;
            }
            else
            {
                min = second.Current as int? ?? int.MaxValue;
                second.MoveNext();
                index2++;
            }
            return min;
        }

        public MyLinkedList2[] Divide()
        {
            MyLinkedList2 aliquot = null;
            MyLinkedList2 aliquant = null;

            var current = Root;
            while (current != null)
            {
                if(current.Value % 3 == 0)
                {
                    if (aliquot == null)
                        aliquot = new MyLinkedList2(new[] { current.Value });
                    else aliquot.Insert(current.Value);
                }
                else
                {
                    if (aliquant == null)
                        aliquant = new MyLinkedList2(new[] { current.Value });
                    else aliquant.Insert(current.Value);
                }
                current = current.Next;
            }
            return new[] { aliquot ?? new MyLinkedList2(new int[0]), aliquant ?? new MyLinkedList2(new int[0]) };
        }

        public MyLinkedList2 NewList()
        {
            var first = Root;
            var last = Tail;
            MyLinkedList2 result = null;
            while(first?.Value <= last?.Value)
            {
                if (result == null)
                    result = new MyLinkedList2(new[] { first.Value * last.Value });
                else result.Insert(first.Value * last.Value);
                first = first.Next;
                if (first == last) break;
                last = last.Prev;
            }
            return result ?? new MyLinkedList2(new int[0]);
        }

        public IEnumerator GetEnumerator()
        {
            var current = Root;
            while (current != null)
            {
                yield return current.Value;
                current = current.Next;
            }
        }

        public override string ToString()
        {
            StringBuilder result = new StringBuilder();
            var current = Root;
            while (current != null)
            {
                result.Append($"{current.Value} ");
                current = current.Next;
            }
            return result.ToString();
        }
    }
}