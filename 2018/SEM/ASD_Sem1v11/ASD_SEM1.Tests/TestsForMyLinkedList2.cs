﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LinkedList2;
using System.Text;

namespace ASD_SEM1.Tests
{
    [TestClass]
    public class LinkedListTests
    {
        [TestMethod]
        public void TestConstructorEmptyArray()
        {
            Assert.AreEqual("", new MyLinkedList2(new int[0]).ToString());
        }

        [TestMethod]
        public void RandomTestConstructor()
        {
            var original = GetRandomList();
            MyLinkedList2 list = new MyLinkedList2(original.ToArray());
            Assert.AreEqual(GetString(original), list.ToString());
            Assert.AreEqual(original.Count, list.Count);
        }

        [TestMethod]
        public void RandomTestInsert()
        {
            List<int> original = GetRandomList().ToList();
            MyLinkedList2 tempList = new MyLinkedList2(original.ToArray());
            int randomNumber = new Random().Next();
            original.Add(randomNumber);
            tempList.Insert(randomNumber);
            Assert.AreEqual(GetString(original), tempList.ToString());
            Assert.AreEqual(original.Count, tempList.Count);
        }

        [TestMethod]
        public void RandomTestDivide()
        {
            var original = GetRandomList();
            var result = new MyLinkedList2(original.ToArray()).Divide();
            int countMod3 = original.Where(x => x % 3 == 0).Count();
            Assert.AreEqual(countMod3, result[0].Count);
            Assert.AreEqual(original.Count - countMod3, result[1].Count);
        }

        [TestMethod]
        public void RandomTestMerge()
        {
            var first = GetRandomList();
            var second = GetRandomList();
            var result = new MyLinkedList2(first.ToArray()).Merge(new MyLinkedList2(second.ToArray()));
            first.AddRange(second);
            Assert.AreEqual(GetString(first), result.ToString());
            Assert.AreEqual(first.Count, result.Count);
        }

        [TestMethod]
        public void RandomTestNewList()
        {
            var original = GetRandomList();
            List<int> right = new List<int>();
            var result = new MyLinkedList2(original.ToArray()).NewList();

            original.Sort();
            var middle = original.Count / 2 + original.Count % 2;
            for (int i = 0; i < middle; i++)
                right.Add(original[i] * original[original.Count - i - 1]);
            right.Sort();

            Assert.AreEqual(GetString(right), result.ToString());
        }

        [TestMethod]
        public void TestNewListEmpty()
        {
            Assert.AreEqual("", new MyLinkedList2(new int[0]).NewList().ToString());
        }

        [TestMethod]
        public void TestNewListEvenCount()
        {
            Assert.AreEqual("6 10 ", new MyLinkedList2(new[] { 1, 5, 6, 2 }).NewList().ToString());
        }

        [TestMethod]
        public void TestNewListOddCount()
        {
            Assert.AreEqual("16 36 ", new MyLinkedList2(new[] { 8, 6, 2 }).NewList().ToString());
        }

        [TestMethod]
        public void TestDivideAllElementsAliquot()
        {
            int[] original = new[] { 3, 6, 99, 33, 66, 27 };
            var result = new MyLinkedList2(original).Divide();
            Assert.AreEqual(original.Length, result[0].Count);
            Assert.AreEqual(0, result[1].Count);
        }

        [TestMethod]
        public void TestDivideAllElementsAliquant()
        {
            int[] original = new[] { 1, 2, 4, 5, 7, 8, 22, 44, 52, 86 };
            var result = new MyLinkedList2(original).Divide();
            Assert.AreEqual(0, result[0].Count);
            Assert.AreEqual(original.Length, result[1].Count);
        }

        [TestMethod]
        public void TestDivideEmptyArray()
        {
            int[] origianl = new int[0];
            var result = new MyLinkedList2(origianl).Divide();
            Assert.AreEqual(0, result[0].Count);
            Assert.AreEqual(0, result[1].Count);
        }

        [TestMethod]
        public void TestMergeOneEmptyArray()
        {
            Assert.AreEqual("1 2 ", new MyLinkedList2(new int[0]).Merge
                (new MyLinkedList2(new[] { 1, 2 })).ToString());
        }

        [TestMethod]
        public void TestMergeTwoEmptyArrays()
        {
            Assert.AreEqual("", new MyLinkedList2(new int[0]).Merge
                (new MyLinkedList2(new int[0])).ToString());
        }

        [TestMethod]
        public void TestMergeBadArray()
        {
            Assert.AreEqual("1 2 3 4 ", new MyLinkedList2(new[] { 1, 2 }).Merge(
                new MyLinkedList2(new[] { 3, 4 })).ToString());
        }

        [TestMethod]
        public void TestInsertFirst()
        {
            int[] original = new[] { 3, 5, 4 };
            var result = new MyLinkedList2(original);
            result.Insert(1);
            Assert.AreEqual("1 3 4 5 ", result.ToString());
            Assert.AreEqual(4, result.Count);
        }

        [TestMethod]
        public void TestInsertLast()
        {
            int[] original = new[] { 3, 5, 4 };
            var result = new MyLinkedList2(original);
            result.Insert(7);
            Assert.AreEqual("3 4 5 7 ", result.ToString());
            Assert.AreEqual(4, result.Count);
        }

        [TestMethod]
        public void TestDeleteFirst()
        {
            int[] original = new[] { 3, 5, 4, 3 };
            var result = new MyLinkedList2(original);
            result.Delete(3);
            Assert.AreEqual("4 5 ", result.ToString());
            Assert.AreEqual(2, result.Count);
        }

        [TestMethod]
        public void TestDeleteLast()
        {
            int[] original = new[] { 3, 5, 4 };
            var result = new MyLinkedList2(original);
            result.Delete(5);
            Assert.AreEqual("3 4 ", result.ToString());
            Assert.AreEqual(2, result.Count);
        }

        [TestMethod]
        public void RandomTestDelete()
        {
            List<int> original = GetRandomList();
            MyLinkedList2 tempList = new MyLinkedList2(original.ToArray());
            int randomNumber = new Random().Next();
            original.Remove(randomNumber);
            tempList.Delete(randomNumber);
            Assert.AreEqual(GetString(original), tempList.ToString());
            Assert.AreEqual(original.Count, tempList.Count);
        }

        [TestMethod]
        public void RandomTestMaxValueCount()
        {
            var original = GetRandomList();
            int current = 1, max = 1;
            for (int i = 1; i < original.Count; i++)
            {
                if (original[i] == original[i - 1])
                    current++;
                else
                    current = 1;
                max = Math.Max(current, max);
            }
            Assert.AreEqual(max, new MyLinkedList2(original.ToArray()).MaxValue());
        }

        private List<int> GetRandomList()
        {
            Random random = new Random();
            List<int> result = new List<int>();
            int size = random.Next(100);
            for (int i = 0; i < size; i++)
                result.Add(random.Next());
            return result;
        }

        private string GetString(List<int> original)
        {
            original.Sort();
            StringBuilder result = new StringBuilder();
            foreach (var t in original)
                result.Append($"{t} ");
            return result.ToString();
        }
    }
}
