﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASDSem2
{
    class ListHeapSort
    {
        public int IterationsCount { get; private set; }
        private int heapSize;

        private void Heapify(List<int> arr, int index)
        {
            int left = 2 * index;
            int right = 2 * index + 1;
            int largest = index;
            IterationsCount++;

            if (left <= heapSize && arr[left] > arr[index])
                largest = left;

            if (right <= heapSize && arr[right] > arr[largest])
                largest = right;

            if (largest != index)
            {
                Swap(arr, index, largest);
                Heapify(arr, largest);
            }
        }

        private void Swap(List<int> arr, int x, int y)
        {
            int temp = arr[x];
            arr[x] = arr[y];
            arr[y] = temp;
        }

        private void BuildHeap(List<int> arr)
        {
            heapSize = arr.Count - 1;
            for (int i = heapSize / 2; i >= 0; i--)
                Heapify(arr, i);
        }

        public void PerformHeapSort(List<int> arr)
        {
            BuildHeap(arr);
            for (int i = arr.Count - 1; i >= 0; i--)
            {
                Swap(arr, 0, i);
                heapSize--;
                Heapify(arr, 0);
            }
        }
    }
}
