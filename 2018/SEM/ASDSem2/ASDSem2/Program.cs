﻿/*
 * Пирамидальная сортировка.
 * Важно: установить рабочую директорию в местоположение program.cs
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASDSem2
{
    class Program
    {
        const int _count = 75;

        static void Main()
        {
            //RandomArrays.WriteArrays(_count);
            var arrays = Reader.ReadArrays(_count);
            var lists = Reader.ReadLists(_count);
            var linkedLists = Reader.ReadLinkedLists(_count);

            Timer timer = new Timer("linked list");
            foreach (var list in linkedLists)
                PerformHeapsortAtLinkedList(list, timer);

            timer = new Timer("array");
            foreach (var arr in arrays)
                PerformHeapsortAtArray(arr, timer);

            timer = new Timer("list");
            foreach (var list in lists)
                PerformHeapsortAtList(list, timer);
        }

        static void PerformHeapsortAtArray(int[] arr, Timer timer)
        {
            ArrayHeapSort sorter = new ArrayHeapSort();
            timer.Restart(arr.Length);
            sorter.PerformHeapSort(arr);
            timer.WriteTimeAtFile(sorter.IterationsCount);
        }

        static void PerformHeapsortAtList(List<int> list, Timer timer)
        {
            ListHeapSort sorter = new ListHeapSort();
            timer.Restart(list.Count);
            sorter.PerformHeapSort(list);
            timer.WriteTimeAtFile(sorter.IterationsCount);
        }

        static void PerformHeapsortAtLinkedList(LinkedList<int> list, Timer timer)
        {
            ArrayHeapSort sorter = new ArrayHeapSort();
            timer.Restart(list.Count);
            sorter.PerformHeapSort(list.ToArray());
            timer.WriteTimeAtFile(sorter.IterationsCount);
        }
    }
}
