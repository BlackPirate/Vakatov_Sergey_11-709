﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using System.IO;

namespace ASDSem2
{
    class Timer
    {
        int length;
        string CollectionName;
        public readonly Stopwatch timer;

        public Timer(string collection)
        {
            this.CollectionName = collection;

            if (File.Exists($"sort {collection}.txt"))
                File.Delete($"sort {collection}.txt");

            timer = new Stopwatch();
        }

        public void Restart(int length)
        {
            this.length = length;
            timer.Restart();
        }

        public void WriteTimeAtFile(int iterations)
        {
            timer.Stop();
            File.AppendAllLines($"sort {CollectionName}.txt",
                new[] { $"{length} {timer.Elapsed.TotalMilliseconds} {iterations}" });
        }
    }
}
