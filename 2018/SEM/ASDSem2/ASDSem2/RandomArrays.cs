﻿using System;
using System.IO;
using System.Text;

namespace ASDSem2
{
    static class RandomArrays
    {
        public static void WriteArrays(int count = 50)
        {
            for (int i = 0; i < count; i++)
                File.WriteAllLines($"arrayRand{i}.txt", new[] { GetRandomArrayToString(random.Next(100, 10000)) });
            File.WriteAllLines("arrayInc.txt", new[] { GetIncreasingArrayToString(10000) });
            File.WriteAllLines("arrayDec.txt", new[] { GetDecreasingArrayToString(10000) });
        }

        private static Random random = new Random();

        private static string GetRandomArrayToString(int count)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < count; i++)
                result.Append($"{random.Next(int.MinValue, int.MaxValue)} ");
            return result.ToString();
        }

        private static string GetDecreasingArrayToString(int size)
        {
            StringBuilder result = new StringBuilder();
            for (int i = size - 1; i >= 0; i--)
                result.Append($"{i} ");
            return result.ToString();
        }

        private static string GetIncreasingArrayToString(int size)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 0; i < size; i++)
                result.Append($"{i} ");
            return result.ToString();
        }
    }
}
