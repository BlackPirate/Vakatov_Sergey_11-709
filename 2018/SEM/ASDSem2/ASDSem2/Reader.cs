﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ASDSem2
{
    class Reader
    {
        public static IEnumerable<int[]> ReadArrays(int count)
        {
            for (int i = 0; i < count; i++)
                yield return GetArray($"arrayRand{i}.txt");
            yield return GetArray("arrayInc.txt");
            yield return GetArray("arrayDec.txt");
        }

        public static IEnumerable<List<int>> ReadLists(int count)
        {
            for (int i = 0; i < count; i++)
                yield return GetList($"arrayRand{i}.txt");
            yield return GetList("arrayInc.txt");
            yield return GetList("arrayDec.txt");
        }

        public static IEnumerable<LinkedList<int>> ReadLinkedLists(int count)
        {
            for (int i = 0; i < count; i++)
                yield return GetLinkedList($"arrayRand{i}.txt");
            yield return GetLinkedList("arrayInc.txt");
            yield return GetLinkedList("arrayDec.txt");
        }

        static int[] GetArray(string fileName) => ParseFile(fileName).ToArray();

        static List<int> GetList(string fileName) => ParseFile(fileName).ToList();

        static LinkedList<int> GetLinkedList(string fileName) => ParseFile(fileName).ToLinkedList();

        static IEnumerable<int> ParseFile(string fileName)
        {
            return File.ReadAllLines(fileName)
                .First()
                .Split(' ')
                .Where(t => t != "")
                .Select(t => int.Parse(t));
        }
    }

    static class EnumerableExt
    {
        public static LinkedList<T> ToLinkedList<T>(this IEnumerable<T> original)
        {
            LinkedList<T> result = new LinkedList<T>();
            foreach (var t in original)
                result.AddLast(t);
            return result;
        }
    }
}
