﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Program
{
    static void FibbonachiTask()
    {
        int[,] baseMatrix = { { 1, 1 }, { 1, 0 } };
        int num = int.Parse(Console.ReadLine());
        Console.WriteLine(PowerRecurrent(baseMatrix,num)[0,0]);
        Console.WriteLine(PowerByCycle(baseMatrix, num)[0,0]);
    }

    static int[,] PowerRecurrent(int[,] value, int power)
    {
        if (power <= 1)
        {
            return value;
        }
        else
        {
            if (power % 2 != 0)
                return MultMatrix(PowerRecurrent(value, power - 1),value);
            else
            {
                int[,] tempResult = PowerRecurrent(value, power / 2);
                return MultMatrix(tempResult, tempResult);
            }
        }
    }

    static int[,] PowerByCycle(int[,] value, int power)
    {
        power--;
        int[,] result = value;
        while (power >= 1)
        {
            if (power % 2 != 0)
            {
                power--;
                result = MultMatrix(result, value);
            }

            value = MultMatrix(value, value);
            power /= 2;
        }
        return result;
    }

    static int[,] MultMatrix(int[,] first, int[,] second)
    {
        int[,] result = new int[2, 2];
        for (int i = 0; i < 2; i++)
            for (int j = 0; j < 2; j++)
                result[i, j] = first[i, 0] * second[0, j] + first[i, 1] * second[1, j];
        return result;
    }

    //////////////////////////////////////////

    static bool possible = false;

    static void Main()
    {
        int n = int.Parse(Console.ReadLine());
        Solve(new int[n,n], n, n);
        Console.WriteLine(possible ? "Да" : "Нет");
    }

    static bool Check(int[,] field, int i, int j)
    {
        int n = field.GetLength(0);
        var sum = 0;
        for (int a = 0; a<n; a++) {
            sum += field[i, a];
            sum += field[a, j];
            for (int t = 0; t < n; t++)
            {
                if (a + t == j + i)
                    sum +=field[a, t];
                if (Math.Abs(i - a) == Math.Abs(j - t))
                    sum += field[a, t];
            }
        }
        return sum < 10;
    }

    static void Solve(int[,] field, int count, int n)
    {
        if (count == 0)
            possible = true;
        for (int i = 0; i < n; i++)
        {
            if (Check(field, n - count, i))
            {
                field[n - count, i] = 10;
                Solve(field, count - 1, n);
            }
        }
    }
}
