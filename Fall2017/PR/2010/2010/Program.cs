﻿using System;

class Program
{
    static void Main()
    {
        var arr = new int[10];
        int sum = 0;
        int middle = 0;
        int count = 0;
        for (int i = 0; i<arr.Length; i++)
        {
            if (arr[i] > 0)
            {
                count++;
                middle += arr[i];
            }
            while (arr[i] > 0)
            {
                sum += arr[i] % 10;
                arr[i] /= 10;
            }
        }
        middle /= count;
    }
}