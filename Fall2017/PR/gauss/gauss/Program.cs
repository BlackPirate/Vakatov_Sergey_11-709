﻿using System;

class Program
{
    static void Main()
    {
        double beforeDet = 1;
        int now = 0;

        Console.WriteLine("a11*x1-a1n*xn, inp = N");
        int n = Convert.ToInt32(Console.ReadLine());

        int[] tempArr = new int[n];
        int[][] matrix = new int[n][];
        for (int i = 0; i < n; i++)
            matrix[i] = new int[n];

        Console.WriteLine("a11*x1-a1n*xn, inp = a11-ann");
        for (int i = 0; i < n; i++)
        {
            string[] temp = Console.ReadLine().Split(new Char[] { ' ' });
            for (int j = 0; j < n; j++)
                matrix[i][j] = Convert.ToInt32(temp[j]);
        }

        //Console.WriteLine("before Det: (1, если его нет)");
        //beforeDet = Convert.ToDouble(Console.ReadLine());

        //////////////////////////////////////////////////////////
        int nod = 1;
        int nok = 1;
        for (int k = 0; k < n; k++)
        {
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                    tempArr[j] = Math.Abs(matrix[i][j]);
                nod = GetNOD(tempArr);
                for (int j = 0; j < n; j++)
                    matrix[i][j] /= nod;
                beforeDet *= nod;
                //Console.WriteLine(String.Format("beforeDet * {0}", nod));
            }

            for (int j = 0; j < n; j++)
            {
                if (matrix[j][now] != 0)
                    tempArr[j] = Math.Abs(matrix[j][now]);
                else tempArr[j] = 1;
            }
            nok = GetNOK(tempArr);

            for (int i = now; i < n; i++)
            {
                if (matrix[i][now] != 0)
                {
                    beforeDet /= (double)nok / matrix[i][now];
                    //`Console.WriteLine(String.Format("beforeDet / {0}", ((double)nok / matrix[i][now])));
                    for (int j = matrix[i].Length - 1; j >= 0; j--)
                    {
                        matrix[i][j] *= nok / matrix[i][now];
                    }
                }
            }

            for (int i = now + 1; i < n; i++)
            {
                if (matrix[now][now] + matrix[i][now] != 0)
                {
                    for (int j = 0; j < n; j++)
                        matrix[now][j] *= -1;
                    beforeDet *= -1;
                }
                for (int j = now; j < n; j++)
                {
                    matrix[i][j] += matrix[now][j];
                }
            }
            now++;
        }

        double det = 1;
        for (int i = 0; i < n; i++)
            det *= matrix[i][i];
        det *= beforeDet;
        Console.WriteLine(det.ToString());
        Console.ReadKey();
    }

    static int GetNOD(int[] line)
    {
        for (int j = 1; j < line.Length; j++)
        {
            while (line[j] * line[j - 1] != 0)
            {
                if (line[j] > line[j - 1])
                    line[j] %= line[j - 1];
                else line[j - 1] %= line[j];
            }
            line[j] += line[j - 1];
        }
        return line[line.Length - 1];
    }

    static int GetNOK(int[] line)
    {
        int nod = 1;
        for (int j = 1; j < line.Length; j++)
        {
            int[] tempArr = { line[j], line[j - 1] };
            line[j] *= line[j - 1];
            nod = GetNOD(tempArr);
            line[j] /= nod;
        }
        return line[line.Length - 1];
    }
}