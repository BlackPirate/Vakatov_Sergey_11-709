﻿using System;

class Program
{
    static void First()
    {
        int x = Convert.ToInt32(Console.ReadLine());
        int y = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine((x + 2 * x * 134 * y - x * y).ToString());
    }

    static void Second()
    {
        int x = Convert.ToInt32(Console.ReadLine());
        int y = Convert.ToInt32(Console.ReadLine());
        int z = Convert.ToInt32(Console.ReadLine());

        if (x > y && x > z)
            if (y > z)
                Console.WriteLine(z.ToString() + ' ' + y.ToString() + ' ' + x.ToString());
            else Console.WriteLine(y.ToString() + ' ' + z.ToString() + ' ' + x.ToString());
        else if (y > x && y > z)
            if (x > z)
                Console.WriteLine(z.ToString() + ' ' + x.ToString() + ' ' + y.ToString());
            else Console.WriteLine(x.ToString() + ' ' + z.ToString() + ' ' + y.ToString());
        else if (x > y)
            Console.WriteLine(y.ToString() + ' ' + x.ToString() + ' ' + z.ToString());
        else Console.WriteLine(x.ToString() + ' ' + y.ToString() + ' ' + z.ToString());
    }

    static void Five()
    {
        int i = 1;                      //простое число
        bool simple = true;             //явяется ли i простым

        while (true)                    //выход из цикла по условию
        {
            i++;
            for (int j = 1; j < i; j++)
            {
                if (i % j == 0 && j != 1)
                {
                    //проверяем наличие делителей
                    simple = false;
                    break;
                }
                if (j * j >= i) break;      //дальше нет смысла искать
            }
            if (simple) Console.WriteLine(i.ToString());

            simple = true;                  //даем новому числу шанс быть простым
        }
    }

    static void Thirth()
    {
        int x = Convert.ToInt32(Console.ReadLine());
        int y = Convert.ToInt32(Console.ReadLine());

        if (x * x - y * y >= 0) Console.WriteLine("Yes");
        else Console.WriteLine("No");
    }

    static void Four()
    {
        int s = 0;
        int k = Convert.ToInt32(Console.ReadLine());

        while (k > 0)
        {
            s += k % 10;
            k /= 10;
        }

        Console.WriteLine(s.ToString());
    }

    static void Six()
    {
        int year = Convert.ToInt32(Console.ReadLine());
        if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) Console.WriteLine("366");
        else Console.WriteLine("365");
    }

    static void Main()
    {
        Console.WriteLine("Введите номер задачи");
        int n = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine();
        switch (n)
        {
            case 1:
                First();
                break;
            case 2:
                Second();
                break;
            case 3:
                Thirth();
                break;
            case 4:
                Four();
                break;
            case 5:
                Five();
                break;
            case 6:
                Six();
                break;
        }

        Console.ReadKey();
    }
}