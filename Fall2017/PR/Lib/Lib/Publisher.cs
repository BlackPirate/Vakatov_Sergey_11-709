﻿namespace Lib
{
    public class Publisher
    {
        public string Name { get; set; }
        public Country PublisherCountry { get; set; }

        public Publisher(string name, Country country)
        {
            this.Name = name;
            this.PublisherCountry = country;
        }

        public int PrintBook(Book book, int instancesCount)
        {
            book.BookPublisher = this;
            return instancesCount;
        }
    }
}