﻿namespace Lib
{
    public class Autor
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public Country AutorCountry { get; set; }

        public Autor(string name, int age, Country country)
        {
            this.Name = name;
            this.Age = age;
            this.AutorCountry = country;
        }

        public Book WriteBook(string name, int pageCount, int year, string genre)
        {
            return new Book(year, pageCount, name, genre, this, null);
        }
    }
}