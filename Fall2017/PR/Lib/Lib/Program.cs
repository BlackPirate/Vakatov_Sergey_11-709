﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib
{
    class Program
    {
        static void Main(string[] args)
        {
            Country country = new Country("Russian", new Coordinates { X = 0, Y = 0 });
            Publisher publisher = new Publisher("Publisher", country);
            Autor autor = new Autor("Autor", 22, country);
            var book = autor.WriteBook("Book", 100, 1999, "Genre");
            book.Burn();
            Console.WriteLine(book.ToString());
        }
    }
}
