﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lib
{
    public class Book
    {
        public int Year { get; set; }
        public int PageCount { get; set; }
        public Autor BookAutor { get; set; }
        public string Name { get; set; }
        public Publisher BookPublisher { get; set; }
        public string Genre { get; set; }
        public bool Exist { get; set; }

        public Book(int year, int pages, string name, string genre, 
            Autor autor, Publisher publister)
        {
            this.Year = year;
            this.PageCount = pages;
            this.Name = name;
            this.Genre = genre;
            this.BookAutor = autor;
            this.BookPublisher = publister;
            this.Exist = true;
        }

        public void Burn()
        {
            this.Exist = false;
        }

        public override string ToString()
        {
            return this.Name;
        }
    }
}
