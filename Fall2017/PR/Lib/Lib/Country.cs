﻿namespace Lib
{
    public class Country
    {
        public string Name { get; set; }
        public Coordinates Position { get; set; }

        public Country(string name, Coordinates position)
        {
            this.Name = name;
            this.Position = position;
        }
    }

    public struct Coordinates
    {
        public int X;
        public int Y;
    }
}