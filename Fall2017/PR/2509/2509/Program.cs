﻿using System;

class Program
{
    static void Main()
    {
        int n = Convert.ToInt32(Console.ReadLine());
        int[] array = new int[n];
        int max=0, sum=0, countPlus=0, countMinus=0;

        for (int i=0; i<array.Length; i++)
        {
            array[i] = Convert.ToInt32(Console.ReadLine());
            sum += array[i];

            if (array[i] >= max || i == 0)
                max = array[i];
            if (array[i] < 0)
            {
                countMinus++;
            }
            else if (array[i]!=0)
            {
                countPlus++;
            }
            
        }
        Console.WriteLine("максимальный элемент:");
        Console.WriteLine(max.ToString());
        Console.WriteLine("сумма:");
        Console.WriteLine(sum.ToString());

        if (countPlus < countMinus)
        {
            Console.WriteLine("количество отрицательных больше, их:");
            Console.WriteLine(countMinus.ToString());
        }
        else if (countPlus != countMinus)
        {
            Console.WriteLine("количество положительных больше, их:");
            Console.WriteLine(countPlus.ToString());
        }
        else Console.WriteLine(String.Format("количество положительных и отрицательных элементов равно: {1}", countPlus));

        Console.ReadKey();
    }
}
