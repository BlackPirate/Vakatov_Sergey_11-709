﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Program
{
    static void Main()
    {
        Console.WriteLine(Task1(5));
        Console.WriteLine(Task2(510));
        Console.WriteLine(Task3("(()))",0,0,0));
        Console.ReadKey();
    }

    static int Task1(int n)
    {
        return n > 0 ? n + Task1(n - 1) : 0;
    }

    static bool Task2(int n)
    {
        if (n == 1)
            return true;
        if ((n & 1) != 0)
            return false;
        return Task2(n / 2);
    }

    static bool Task3(string line, int index, int countOpen, int countClose)
    {
        if (index >= line.Length)
            return countOpen == countClose;

        if (line[index] == ')')
            countClose++;
        if (line[index] == '(')
            countOpen++;
        return Task3(line, ++index, countOpen, countClose);
    }

    static List<string> Task4(char currentSymbol, int countOpen, int countClose)
    {
        if (countOpen + countClose > 5)
            return null;
    }
}