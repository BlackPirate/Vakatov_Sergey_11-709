﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace list
{
    class Program
    {
        static void Main()
        {
            MyList myList = new MyList();
            myList.root = new Node { Value = 0};
            Node end = new Node { Value = 2, Next = myList.root };
            myList.root.Next = end;

            \myList.SetNode(4, myList.end.Value);
            ChangeMax(myList);
            myList.SetNode(3, myList.end.Value);
            ChangeMax(myList);
            myList.SetNode(-1, myList.end.Value);
            ChangeMax(myList);
        }
        static void ChangeMax(MyList myList)
        {
            var temp = myList.root.Next;
            while (temp.Next != myList.root)
            {
                temp = temp.Next;
            }
            myList.end = temp;
        }
    }
}
