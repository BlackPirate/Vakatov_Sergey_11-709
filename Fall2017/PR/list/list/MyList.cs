﻿namespace list
{
    public class MyList
    {
        public Node root;
        public Node end;
        public void SetValue(int value, Node pre)
        {
            if (value > pre.Value)
            {
                Node temp = pre.Next;
                SetValue(value, temp);
            }
            else
            {
                Node node = new Node
                {
                    Value = value,
                    Next = pre.Next
                };
                pre.Next = node;
            }
        }
        public void SetNode(int value, int maxValue)
        {
            Node temp = this.root;
            while (value > temp.Next.Value || value < this.root.Value)
            {
                temp = temp.Next;
                if (temp.Value == maxValue)
                    break;
            }
            Node node = new Node
            {
                Value = value,
                Next = temp.Next
            };
            temp.Next = node;
        }
    }
}