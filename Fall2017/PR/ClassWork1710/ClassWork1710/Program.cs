﻿using System;

class Program
{
    static void Main()
    {
        Sum();
        string a = Console.ReadLine();
        string b = Console.ReadLine();
        bool possible = true;

        while (possible)
        {
            for (int j = 0; j < b.Length; j++)
            {
                if (a.Length != 0 && a[0] == b[j])
                {
                    //a = Move(a, 0);
                    a = a.Remove(0);
                    //b = Move(b, j);
                    b = b.Remove(j);
                    possible = true;
                    break;
                }
                possible = false;
            }
        }

        if (a == "")
            Console.WriteLine("да");
        else
            Console.WriteLine("нет");

        Console.ReadKey();
    }

    static string Move(string a, int pos)
    {
        string b = "";
        for (int i = 0; i < pos; i++)
            b += a[i];
        for (int i = pos; i < a.Length - 1; i++)
            b += a[i + 1];
        return b;
    }

    static void Sum()
    {
        int min = -1; int max = -1, sum = 0;
        for (int i = 0; i < 5; i++)
        {
            int a = int.Parse(Console.ReadLine());
            if (i == 0) { min = a; max = a; }
            sum += a;
            if (a > max) max = a;
            if (a < min) min = a;
        }
        Console.WriteLine($"max: {sum - min}");
        Console.WriteLine($"min: {sum - max}");
        Console.ReadKey();
    }
}