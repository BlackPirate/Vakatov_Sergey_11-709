﻿/************************************************
 **         Семестровка 1                      **
 **         Выполнил: Вакатов Сергей           **
 **         Группа: 11-709                     **
 **         Задание 8, часть 4                 **
 ************************************************/
using System;

class Program
{

    static void Main()
    {
        DateTime beg = DateTime.Now;            //время старта программы

        int sum = 0;
        for (int i = 1000; i < 10000; i++)
        {
            if (i == (Math.Pow(i / 1000, 4) + Math.Pow((i / 100) % 10, 4) + Math.Pow((i / 10) % 10, 4) + Math.Pow(i % 10, 4)))
            {
                sum += i;
                //перебором в цикле суммируем все подходящие числа
            }
        }
        Console.WriteLine(sum.ToString());

        DateTime end = DateTime.Now;            //время окончания работы

        Console.WriteLine((end-beg).ToString());
        Console.ReadKey();
    }
}