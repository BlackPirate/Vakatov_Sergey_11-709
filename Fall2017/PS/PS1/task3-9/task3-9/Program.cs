﻿/************************************************
 **         Семестровка 1                      **
 **         Выполнил: Вакатов Сергей           **
 **         Группа: 11-709                     **
 **         Задание 9, часть 3                 **
 ************************************************/
using System;

class Program
{
    static void Main()
    {
        int n = Convert.ToInt32(Console.ReadLine());
        bool signChange = true;
        int a, b;                                           //будем сравнивать только два члена

        a = Convert.ToInt32(Console.ReadLine());

        for (int i = 1; i < n; i++)
        {
            b = Convert.ToInt32(Console.ReadLine());

            //произведение двух положительный или отрицательных чисел всегда больше нуля
            if (a * b > 0)
                signChange = false;

            a = b;                                          //значение нового члена записываем в b, поэтому старому присваиваем а
        }

        if (signChange) Console.WriteLine("знакопеременная");
        else Console.WriteLine("не знакопеременная");

        Console.ReadKey();
    }
}
