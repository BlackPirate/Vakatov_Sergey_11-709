﻿/************************************************
 **         Семестровка 1                      **
 **         Выполник: Вакатов Сергей           **
 **         Группа: 11-709                     **
 **         Задание 12, часть 2                **
 ************************************************/
using System;

class Program
{
    static void Main()
    {
        int k, m;   //основания С.С.
        double a, b;   //сами числа

        Console.WriteLine("введите основание сисиетмы и само число");
        k = Convert.ToInt32(Console.ReadLine());
        a = Convert.ToDouble(Console.ReadLine());

        Console.WriteLine("введите основание сисиетмы и само число");
        m = Convert.ToInt32(Console.ReadLine());
        b = Convert.ToDouble(Console.ReadLine());

        Console.WriteLine(Translate(k, a).ToString() + ' ' + Translate(m, b).ToString());
        Console.ReadKey();
    }

    static double Translate(int n, double o)
    {
        int i = 0;
        double s1 = 0;                      //дробная часть числа в 10й СС
        int s2 = 0;                         //целая часть числа в 10й СС
        int whole = Convert.ToInt32(o);     //получаем целую часть числа

        double a;
        while (i>=-10)
        {
            i--;
            a = (int)((o * Math.Pow(10, -i))%10) * Math.Pow(n,i); //получаем нужную цифру чила в 10й СС
            s1 += a;
        }


        i = 0;

        while (whole > 0)
        {
            s2 += (int)Math.Pow(n, i) * (whole % 10);
            whole /= 10;
            i++;
        }
        return s2+s1;
    }
}