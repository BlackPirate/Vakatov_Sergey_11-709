﻿using System;

class Program
{
    static void Main()
    {
        double x1, x2, x3, y1, y2, y3;      //координаты точек
        double len1, len2, len3;            //длины сторон
        double x, y;                        //координаты нужной точки

        Console.WriteLine("Введите координаты первой точки");
        x1 = double.Parse(Console.ReadLine());
        y1 = double.Parse(Console.ReadLine());
        Console.WriteLine("Введите координаты второй точки");
        x2 = double.Parse(Console.ReadLine());
        y2 = double.Parse(Console.ReadLine());
        Console.WriteLine("Введите координаты третьей точки");
        x3 = double.Parse(Console.ReadLine());
        y3 = double.Parse(Console.ReadLine());

        Console.WriteLine("введите три расстояния, начиная с расстояния от первой точки");
        len1 = double.Parse(Console.ReadLine());
        len2 = double.Parse(Console.ReadLine());
        len3 = double.Parse(Console.ReadLine());

        //решая систему из трех безобидных уравнений окружностей приходим к страшной формуле (она едва на А4 уместилась)
        //идея в следующем: нарисуем окружности указанных радиусов из каждой точки соответсвтенно.
        //все они могут пересекаться только в одной точке - это и есть наш ответ
        //(x-x0)^2+(y-y0)^2=R^2 - составляем три уравнения, раскрываем скобки, вычитаем уравнения
        //этим мы избавились от квадратов, а дальше просто подставляем... так получилаь эта формула
        y = ((x1 - x2) * (x3 * x3 - x1 * x1 + y3 * y3 - y1 * y1 + len1 * len1 - len3 * len3) + (x1 - x3) * (x1 * x1 - x2 * x2 + y1 * y1 - y2 * y2 + len2 * len2 - len1 * len1)) / (2 * ((y1 - y2) * (x1 - x3) + (x1 - x2) * (y3 - y1)));
        x = (x1 * x1 - x2 * x2 + y1 * y1 - y2 * y2 - 2 * y * (y1 - y2) + len2 * len2 - len1 * len1) / (2 * (x1 - x2));

        if (double.IsNaN(x))        //вдруг дадут некорректный случай, когда треугольник построить нельзя
        {   //в этом случае точки лежат на одной прямой
            if (len1 == 0)
            {
                x = x1;
                y = y1;
            }
            else if (len2 == 0)
            {
                x = x2;
                y = y2;
            }
            else
            {
                x = x3;
                y = y3;
            }
        }

        Console.WriteLine(String.Format("Координаты точки: ({0};{1})", x, y));

        Console.ReadKey();
    }
}