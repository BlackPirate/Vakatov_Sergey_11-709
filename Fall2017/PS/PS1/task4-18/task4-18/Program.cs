﻿/************************************************
 **         Семестровка 1                      **
 **         Выполник: Вакатов Сергей           **
 **         Группа: 11-709                     **
 **         Задание 18, часть 4                **
 ************************************************/

using System;

class Program
{
    static void Main()
    {
        Console.WriteLine("введите номер простого числа, которое надо найти");   
        int k = Convert.ToInt32(Console.ReadLine());

        DateTime beg = DateTime.Now;    //начинаем тсчет времени

        int i = 1;                      //простое число
        int count = 0;                  //количество простых чисел
        bool simple = true;             //явяется ли i простым

        while (true)                    //выход из цикла по условию
        {
            i++;
            for (int j = 1; j < i; j++)
            {
                if (i % j == 0 && j != 1)
                {
                    //проверяем наличие делителей
                    simple = false;
                    break;
                }
                if (j * j >= i) break;      //дальше нет смысла искать
            }
            if (simple) count++;            //увеличиваем счетчик, если число простое

            simple = true;                  //даем новому числу шанс быть простым

            if (count == k) break;          //выходим из цикла, если нашли нужное простое число
        }

        Console.WriteLine(i);               //выводим нужное простое число

        DateTime end = DateTime.Now;        //время окончания работы программы
        Console.WriteLine((end - beg).ToString());  //выводим время работы программы
        Console.ReadKey();
    }
}
