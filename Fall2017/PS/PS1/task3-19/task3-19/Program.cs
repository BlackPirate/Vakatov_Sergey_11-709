﻿/************************************************
 **         Семестровка 1                       **
 **         Выполник: Вакатов Сергей           **
 **         Группа: 11-709                     **
 **         Задание 19, часть 3                **
 ************************************************/

using System;


class Program
{
    static void Main()
    {
        int c = Convert.ToInt32(Console.ReadLine());
        string output = "";

        while (c != 0)
        {
            if (c < 0)
                output += (-c).ToString() + ' ';                            //число отрицательное, убираем минус (тот же модуль, но короче)
            else
                output += Math.Ceiling(Math.Log(c, 2)).ToString() + ' ';    //берем логарифм по основанию 2, округляем в бошую сторну

            c = Convert.ToInt32(Console.ReadLine());
        }

        Console.WriteLine(output);
        Console.ReadKey();
    }
}