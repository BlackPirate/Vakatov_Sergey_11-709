﻿/************************************************
 **         Семестровка 1                      **
 **         Выполнил: Вакатов Сергей           **
 **         Группа: 11-709                     **
 **         Задание 2, часть 2                 **
 ************************************************/
using System;


class Program
{
    static void Main()
    {
        Console.WriteLine("введите первый член прогрессии");
        double a1 = Convert.ToDouble(Console.ReadLine());

        Console.WriteLine("введите второй член прогрессии");
        double a2 = Convert.ToDouble(Console.ReadLine());

        Console.WriteLine("введите номер члена прогресии, который нужно найти");
        int k = Convert.ToInt32(Console.ReadLine());

        double d = a2 - a1;

        double ak = a1 + d*(k - 1);         //всем известная формула для нахождения члена прогрессии

        Console.WriteLine(ak.ToString());
        Console.ReadKey();
    }
}
