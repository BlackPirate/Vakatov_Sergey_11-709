﻿/////////////////////////////////////////////////////////
///           Задания 18,5,3                          ///
///           Выполнил Вакатов Сергей                 ///
///           Группа 11-709                           ///
/////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task1
{
    class Program
    {
        static void Task3()
        {
            //Task3, sin(x)
            double x = double.Parse(Console.ReadLine());
            var result = GetSumTask3(x);
            Console.WriteLine(result.Item1);
            Console.WriteLine(result.Item2);
        }

        static Tuple<double, int> GetSumTask3(double x, double epsilon = 1e-6)
        {
            double first = x, second = x * x * x / 6;
            int k = first > epsilon ? (second > epsilon ? 2 : 1) : 0;
            double result = first - second;
            bool evenPow = true;
            while (first - second > epsilon)
            {
                first = second;
                second *= x * x / (2 * k * (2 * k + 1));
                result += second * (evenPow ? 1 : -1);
                k++;
                evenPow = !evenPow;
            }
            return Tuple.Create<double, int>(result, k);
        }

        static void Task2()
        {
            //Task5 ln(1+x)
            double x = double.Parse(Console.ReadLine());
            var result = GetSumTask2(x);
            Console.WriteLine(result.Item1);
            Console.WriteLine(result.Item2);
        }

        static Tuple<double, int> GetSumTask2(double x, double epsilon = 1e-6)
        {
            double first = x, second = x * x / 2;
            double result = first - second;
            int k = first > epsilon ? (second > epsilon ? 2 : 1) : 0;
            bool evenPow = true;
            while (first - second >= epsilon)
            {
                k++;
                first = second;
                second *= x / k;
                result += second * (evenPow ? 1 : -1);
                evenPow = !evenPow;
            }
            return Tuple.Create<double, int>(result, k);
        }

        static void Task1()
        {
            //Task18, Константа Каталана
            double result = (Math.PI / 8) * Math.Log(Math.Sqrt(3) + 2);
            var tempResult = GetSumTask1(1e-18);
            result += (3.0 / 8) * tempResult.Item1;
            Console.WriteLine(result);
            Console.WriteLine(tempResult.Item2);
        }

        static Tuple<double, int> GetSumTask1(double epsilon = 1e-6)
        {
            double first = 1, second = 1.0 / 18;
            double result = second + first;
            int n = first > epsilon ? (second > epsilon ? 2 : 1) : 0;
            while (first - second >= epsilon)
            {
                first = second;
                second *= (2.0 * n - 1) * n / (2 * (2 * n + 1) * (2 * n + 1));
                result += second;
                n++;
            }
            return Tuple.Create<double, int>(result, n);
        }
    }
}
