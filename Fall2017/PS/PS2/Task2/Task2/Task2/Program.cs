﻿/////////////////////////////////////////////////////////
///           Задание 5, ч.2                          ///
///           Выполнил Вакатов Сергей                 ///
///           Группа 11-709                           ///
/////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Program
{
    static void Main()
    {
        //Task5
        double result = -2 + 2 * GetPiecePi();
        Console.WriteLine(result);
    }

    static double GetPiecePi(double epsilon = 1e-18)
    {
        double first = 1, second = 2.0 / 3;
        double result = first + second;
        int k = 2;
        while (first - second >= epsilon)
        {
            k++;
            first = second;
            second *= k / (2.0 * k - 1);
            result += second;
        }

        return result;
    }
}
