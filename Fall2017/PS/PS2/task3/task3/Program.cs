﻿/////////////////////////////////////////////////////////
///           Задание 4, часть 3                      ///
///           Выполнил Вакатов Сергей                 ///
///           Группа 11-709                           ///
/////////////////////////////////////////////////////////
using System;

class Program
{
    const int Right = 3;
    const int Left = 2;

    static void Main()
    {
        int countStep = 10000000;
        Console.WriteLine(MonteKarlo(countStep, 5));
        Console.WriteLine(LeftRectangle(countStep));
        Console.WriteLine(RightRectangle(countStep));
        Console.WriteLine(Trapezies(countStep));
        Console.WriteLine(Sympson(countStep));
    }

    static double Function(double position)
    {
        return -Math.Sin(Math.Tan(position));
    }

    static double LeftRectangle(int countSteps)
    {
        double result = 0;
        double step = (Right - Left) / (double)countSteps;

        for (int i = 0; i < countSteps; i++)
            result += Function(Left + step * i);

        result *= step;
        return result;
    }

    static double RightRectangle(int countSteps)
    {
        double result = 0;
        double step = (Right - Left) / (double)countSteps;

        for (int i = 1; i <= countSteps; i++)
            result += Function(Left + step * i);

        result *= step;
        return result;
    }

    static double Trapezies(int countSteps)
    {
        double result = 0;
        double step = (Right - Left) / (double)countSteps;

        for (int i = 0; i < countSteps; i++)
            result += (Function(Left + step * i) + Function(Left + step * (i + 1))) / 2;

        result *= step;
        return result;
    }

    static double Sympson(int countSteps)
    {
        double result = 0;
        double step = (Right - Left) / (double)countSteps;
        bool swicher = true;

        for (int i = 1; i < countSteps; i++)
        {
            result += (swicher ? 4 : 2) * Function(Left + step * i);
            swicher = !swicher;
        }

        result += Function(Right) + Function(Left);
        result *= step / 3;
        return result;
    }

    static double MonteKarlo(int countSteps, int height = 2)
    {
        var random = new Random();
        int count = 0;
        for (int i = 0; i < countSteps; i++)
        {
            double x = random.NextDouble() + Left;
            double y = random.NextDouble() * height;
            if (y <= Function(x))
                count++;
        }
        return height * (double)count/countSteps;
    }
}