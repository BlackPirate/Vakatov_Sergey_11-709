﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
namespace task4
{
    public class NewDouble
    {
        //Целая часть числа
        public List<sbyte> whole = new List<sbyte>();
        //Дробная часть
        public List<sbyte> fraction = new List<sbyte>();
        //знак числа. если минус, то true
        public bool sign;

        public NewDouble(string str = "0.0")
        {
            if (str[0] == '-')
            {
                sign = true;
                str = str.Substring(1, str.Length - 1);
            }
            else sign = false;

            string[] temp = str.Split('.');
            whole = SetNumber(temp[0]);
            fraction = SetNumber(temp.Length != 1 ? temp[1] : "0");
        }   

        private List<sbyte> SetNumber(string num)
        {
            List<sbyte> arr = new List<sbyte>();

            for (int i = 0; i < num.Length; i++)
            {
                arr.Add(sbyte.Parse(num[i].ToString()));
            }

            return arr;
        }

        //Главный метод для умножения
        public NewDouble Multiplication(NewDouble second)
        {
            NewDouble result = new NewDouble();

            List<sbyte> temp = MultPiece(GetNumberInList(this), GetNumberInList(second));

            int pointNumber = second.fraction.Count + this.fraction.Count;

            result = GetTypeFromList(temp, pointNumber);
            result.whole = RemoveZero(result.whole);
            result.fraction = RemoveZeroEnd(result.fraction);
            result.sign = this.sign ^ second.sign;

            return result;
        }

        private List<sbyte> MultPiece(List<sbyte> first, List<sbyte> second)
        {
            List<sbyte> result = new List<sbyte>();
            int countZero = 0;
            int secondIteration = 0;
            for (int i = first.Count - 1; i >= 0; i--)
            {
                var temp = TempResultOfMult(first, second, i, ref secondIteration);
                for (int j = 0; j < countZero; j++)
                    temp.Add(0);
                result = AddPiece(result, temp, true);
                countZero++;
            }
            return result;
        }

        private List<sbyte> TempResultOfMult(List<sbyte> first, List<sbyte> second, int point, ref int secondIteration)
        {
            int tempMult = 0;
            List<sbyte> temp = new List<sbyte>();
            for (int j = second.Count - 1; j >= 0; j--)
            {
                var firstIteration = (sbyte)(first[point] * second[j]);
                tempMult = (firstIteration + secondIteration / 10);
                temp.Insert(0, (sbyte)(tempMult % 10));
                secondIteration = tempMult;
            }
            if (secondIteration >= 10)
                temp.Insert(0, (sbyte)(secondIteration / 10));
            return temp;
        }

        //Главный метод для деления
        public NewDouble Division(NewDouble second)
        {
            NewDouble result = new NewDouble();

            var template = StepToDivision(second);
            var secondList = GetNumberInList(template[1]);
            var firstList = GetNumberInList(template[0]);

            result.whole = DivideWhole(firstList, secondList);
            result.fraction = RemoveZeroEnd(DivideFraction(firstList, secondList));
            result.sign = this.sign ^ second.sign;

            return result;
        }

        private List<sbyte> DivideWhole(List<sbyte> firstList, List<sbyte> secondList)
        {
            var temp = new List<sbyte> { 0 };
            while (CompareWhole(firstList, secondList) != 1)
            {
                SubstractPiece(secondList, firstList);
                RemoveZero(firstList);
                temp = AddPiece(temp, new List<sbyte> { 1 }, true);
            }
            return temp;
        }

        private List<sbyte> DivideFraction(List<sbyte> firstList, List<sbyte> secondList, int steps = 100)
        {
            var temp = new List<sbyte>();
            while (steps > 0)
            {
                firstList = MultPiece(firstList, new List<sbyte> { 1, 0 });
                if (CompareWhole(firstList, secondList) != 1)
                    temp.Add(DivideWhole(firstList, secondList)[0]);
                else temp.Add(0);
                steps--;
            }
            return temp;
        }

        private NewDouble[] StepToDivision(NewDouble second)
        {
            var template = new[] { this, second };
            for (int i = 0; i < second.fraction.Count; i++)
            {
                template[1] = template[1].Multiplication(new NewDouble("10.0"));
                template[0] = template[0].Multiplication(new NewDouble("10.0"));
                for (int j = 0; j < 2; j++)
                    template[j].fraction.RemoveAt(template[j].fraction.Count - 1);
            }
            return template;
        }

        //удаляет незначащие нули в начале числа
        private List<sbyte> RemoveZero(List<sbyte> original)
        {
            int count = 0;
            for (int i = 0; i < original.Count && original[i] == 0; i++)
                count++;
            original.RemoveRange(0, count);
            if (original.Count == 0)
                return new List<sbyte> { 0, 0 };
            return original;
        }

        //удаляет незначащие нули в конце числа
        private List<sbyte> RemoveZeroEnd(List<sbyte> original)
        {
            for (int i = original.Count - 1; i > 0; i--)
            {
                if (original[i] == 0)
                    original.RemoveAt(i);
                else break;
            }
            return original;
        }

        //главный метод для суммирования
        public NewDouble Addition(NewDouble secondNum)
        {
            NewDouble result = new NewDouble();

            result.sign = this.sign | secondNum.sign;
            result.whole = AddPiece(this.whole, secondNum.whole, true);
            result.fraction = AddPiece(this.fraction, secondNum.fraction, false);

            List<sbyte> temp = new List<sbyte>();
            temp.Add((sbyte)(result.fraction[0] / 10));
            result.fraction[0] %= 10;
            if (temp[0] != 0)
                result.whole = result.AddPiece(result.whole, temp, true);
            return result;
        }

        //складывает два экземпляра List
        private List<sbyte> AddPiece(List<sbyte> firstNum, List<sbyte> secondNum, bool whole)
        {
            int countZero = Math.Abs(firstNum.Count - secondNum.Count);

            int pos;
            if (whole)
                pos = 0;
            else pos = Math.Max(firstNum.Count, secondNum.Count);

            if (firstNum.Count > secondNum.Count)
                secondNum = AddValues(secondNum, countZero, pos, 0);
            else
                firstNum = AddValues(firstNum, countZero, pos, 0);

            List<sbyte> result = AddValues(new List<sbyte>(), secondNum.Count, 0, 0);

            int firstIteration = 0;
            int secondIteration = 0;
            for (int i = firstNum.Count - 1; i >= 0; i--)
            {
                firstIteration = firstNum[i] + secondNum[i];
                result[i] += (sbyte)(firstIteration % 10);
                result[i] += (sbyte)(secondIteration / 10);
                result[i] %= 10;
                secondIteration = firstIteration + secondIteration / 10;
            }
            if (secondIteration >= 10 && pos == 0)
                result.Insert(0, (sbyte)(secondIteration / 10));
            else if (secondIteration >= 10 && pos != 0)
                result[0] += (sbyte)((secondIteration / 10) * 10);
            return result;
        }

        //first < second
        //метод для вычитания одного экзампляра List из другого
        private List<sbyte> SubstractPiece(List<sbyte> first, List<sbyte> second)
        {
            int tempPoint = second.Count - 1;
            for (int i = first.Count - 1; i >= 0; i--)
            {
                second[tempPoint] -= first[i];
                tempPoint--;
            }
            for (int i = second.Count - 1; i > 0; i--)
            {
                if (second[i] < 0)
                {
                    second[i] += 10;
                    second[i - 1]--;
                }
            }
            if (second[0] < 0)
            {
                second[0] += 10;
                second.Insert(0, 100);
            }
            return second;
        }

        //добавляет указанный символ в нужное место нужное количество раз
        private List<sbyte> AddValues(List<sbyte> number, int count, int pos, sbyte value)
        {
            if (pos < count)
                for (int i = 0; i < count; i++)
                    number.Insert(pos, value);
            else
                for (int i = 0; i < count; i++)
                    number.Add(value);
            return number;
        }

        //сравнивает целые части чисел
        private int CompareWhole(List<sbyte> first, List<sbyte> second)
        {
            if (first.Count < second.Count)
                return 1;
            else if (first.Count > second.Count)
                return 0;
            for (int i = 0; i < second.Count; i++)
            {
                if (first[i] > second[i])
                    return 0;
                else if (first[i] < second[i])
                    return 1;
            }

            return -1;
        }

        //сравнивает дробные части
        private int CompareFraction(List<sbyte> first, List<sbyte> second)
        {
            for (int i = 0; i < second.Count; i++)
            {
                if (first[i] > second[i])
                    return 0;
                else if (first[i] < second[i])
                    return 1;
            }

            return -1;
        }

        //возвращает экзампляр класса в виде строки
        public string NumberToString()
        {
            StringBuilder number = new StringBuilder();

            if (this.sign)
                number.Append("-");
            foreach (byte symbol in this.whole)
                number.Append(symbol);
            number.Append(".");
            foreach (byte symbol in this.fraction)
                number.Append(symbol);

            return number.ToString();
        }

        //возвращает экзампляр класса, имея List
        public NewDouble GetTypeFromList(List<sbyte> temp, int pointNumber, bool signTo = false)
        {
            NewDouble result = new NewDouble();
            result.sign = signTo;
            for (int i = 0; i < temp.Count; i++)
            {
                if (i < temp.Count - pointNumber) result.whole.Add(temp[i]);
                else result.fraction.Add(temp[i]);
            }

            result.whole.RemoveAt(0);
            result.fraction.RemoveAt(0);

            return result;
        }

        //переводит экзампляр класса в List
        public List<sbyte> GetNumberInList(NewDouble temp)
        {
            List<sbyte> result = new List<sbyte>();
            foreach (var first in temp.whole)
                result.Add(first);
            foreach (var first in temp.fraction)
                result.Add(first);
            return result;
        }
    }
}