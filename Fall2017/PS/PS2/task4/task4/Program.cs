﻿/////////////////////////////////////////////////////////
///           Задание ?, часть 4                      ///
///           Выполнил Вакатов Сергей                 ///
///           Группа 11-709                           ///
/////////////////////////////////////////////////////////

using System;

namespace task4
{
    class Program
    {
        static void Main()
        {
            /*
             * Реализован класс NewDouble для длинной арифметики
             * Его методы для пары a и b:
             * 1. a.Multiplication( b ) = a*b
             * 2. a.Division( b ) = a/b (деление дробное)
             * 3. a.NumberToString() - представит число в виде строки
             * 
             * Бонус: (не всегда работает)
             * 1. a.Addition (b) = a + b (если не играться с отрицательными, все хорошо)
             */
            NewDouble first = new NewDouble(Console.ReadLine());
            NewDouble second = new NewDouble(Console.ReadLine());

            Console.WriteLine("a / b");
            var result = first.Division(second);
            Console.WriteLine(result.NumberToString());

            Console.WriteLine("a * b");
            result = first.Multiplication(second);
            Console.WriteLine(result.NumberToString());

            Console.ReadKey();
        }
    }
}
