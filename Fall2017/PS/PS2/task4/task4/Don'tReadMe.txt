////����� ��������� ��� ��������� ���� �����, 
        //private List<sbyte>[] ListsToSubstraction(NewDouble second)
        //{
        //    bool tempSign = false;
        //    var firstList = GetNumberInList(this);
        //    var secondList = GetNumberInList(second);

        //    switch (CompareWhole(this.whole, second.whole))
        //    {
        //        case -1:
        //            switch (CompareFraction(this.fraction, second.fraction))
        //            {
        //                case -1:
        //                    return new[] { new List<sbyte>() };
        //                case 0:
        //                    var tt = firstList;
        //                    firstList = secondList;
        //                    secondList = tt;
        //                    tempSign = true ^ this.sign ^ second.sign;
        //                    break;
        //            }
        //            break;
        //        case 0:
        //            var t = firstList;
        //            firstList = secondList;
        //            secondList = t;
        //            tempSign = true ^ this.sign ^ second.sign;
        //            break;
        //    }

        //    int countZero = Math.Abs(this.fraction.Count - second.fraction.Count);
        //    int pos = Math.Max(this.fraction.Count, second.fraction.Count);

        //    if (this.fraction.Count < second.fraction.Count)
        //        firstList = AddValues(firstList, countZero, pos, 0);
        //    else
        //        secondList = AddValues(secondList, countZero, pos, 0);

        //    return new[] { firstList, secondList, new List<sbyte> { (sbyte)(tempSign ? -1 : 1) } };
        //}

        public NewDouble Substraction(NewDouble second)
        {
            NewDouble result = new NewDouble();

            var template = ListsToSubstraction(second);

            if (template.Length == 1)
                return new NewDouble();

            var tempResult = SubstractPiece(template[0], template[1]);
            tempResult = RemoveZero(tempResult);

            int pointNumber = Math.Max(this.fraction.Count, second.fraction.Count);
            result = GetTypeFromList(tempResult, pointNumber);
            if (template[2][0] == -1)
                result.sign = true;
            return result;
        }