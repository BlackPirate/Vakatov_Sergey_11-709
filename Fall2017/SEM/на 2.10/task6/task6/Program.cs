﻿/////////////////////////////////////////////
//          Задание 7, ulearn
//          Вакатов Сергей
//          Группа 11-709
/////////////////////////////////////////////

using System;

class Program
{
    static void Main()
    {
        double x1, x2, x3, x4, y1, y2, y3, y4;
        x1 = double.Parse(Console.ReadLine());
        y1 = double.Parse(Console.ReadLine());
        x2 = double.Parse(Console.ReadLine());
        y2 = double.Parse(Console.ReadLine());
        x3 = double.Parse(Console.ReadLine());
        y3 = double.Parse(Console.ReadLine());

        double lenAB, lenBC, lenAC, d, a;
        lenAB = Math.Sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
        lenAC = Math.Sqrt((x1 - x3) * (x1 - x3) + (y1 - y3) * (y1 - y3));
        lenBC = Math.Sqrt((x3 - x2) * (x3 - x2) + (y3 - y2) * (y3 - y2));

        if (lenAB != lenBC)
        {
            d = Math.Max(lenAB, lenBC);
            a = Math.Min(lenAB, lenBC);
        }
        else
        {
            d = Math.Max(lenAB, lenAC);
            a = Math.Min(lenAB, lenAC);
        }

        if (Math.Abs(d-a*Math.Sqrt(2))<1e-10)
        {
            double x, y;
            if (lenAB == d)
            {
                x = (x1 + x2) / 2;
                y = (y1 + y2) / 2;

                x4 = 2 * x - x3;
                y4 = 2 * y - y3;
            }
            else if (lenAC == d)
            {
                x = (x1 + x3) / 2;
                y = (y1 + y3) / 2;

                x4 = 2 * x - x2;
                y4 = 2 * y - y2;
            }
            else
            {
                x = (x2 + x3) / 2;
                y = (y2 + y3) / 2;

                x4 = 2 * x - x1;
                y4 = 2 * y - y1;
            }
        }
        else
        {
            Console.WriteLine("нет");
            Console.ReadKey();
            return;
        }

        Console.WriteLine(x4.ToString() +" "+ y4.ToString());
        Console.ReadKey();
    }
}
