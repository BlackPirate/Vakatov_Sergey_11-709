﻿using System;

class Program
{
    static void Main()
    {
        int count = 0, i, n;
        n = int.Parse(Console.ReadLine());
        for (i = 1; i <= n; i++)
        {
            count += (int)Math.Log10(i)+1;
            if (count >= n) break;
        }
        i /= (int)Math.Pow(10, count - n);
        Console.WriteLine(i % 10);
    }
}