﻿using System.Collections.Generic;

namespace TableParser
{
    public class FieldsParserTask
    {
        public static List<string> ParseLine(string line)
        {
            var resultList = new List<string>();
            int i = 0;
            while (i < line.Length)
            {
                if (line[i] != ' ')
                {
                    var field = ReadField(line, i);
                    resultList.Add(field.Value);
                    i = field.GetIndexNextToToken();
                }
                else i++;
            }
            return resultList;
        }
        private static Token ReadField(string line, int startIndex)
        {
            if (line[startIndex] != '"' && line[startIndex] != '\'')
                return Simple(line, startIndex);
            else return Quote(line, startIndex);
        }
        static Token Simple(string line, int startIndex)
        {
            int tempIndex = startIndex;
            string tempString = "";
            var ch = line[tempIndex];
            while (tempIndex < line.Length && ch != '"' && ch != '\'' && ch != ' ')
            {
                tempString += line[tempIndex];
                tempIndex++;
                ch = line[tempIndex];
            }
            return new Token(tempString, startIndex, tempIndex - startIndex);
        }
        static Token Quote(string line, int startIndex)
        {
            int tempIndex = startIndex + 1;
            string tempString = "";
            char currentQuote = line[tempIndex - 1];
            while (tempIndex < line.Length)
            {
                if (line[tempIndex] == currentQuote && (line[tempIndex - 1] != '\\' || (line[tempIndex - 1] == '\\' && line[tempIndex - 2] == '\\')))
                    break;
                if (line[tempIndex] != '\\') tempString += line[tempIndex];
                if (line[tempIndex] == '\\' && line[tempIndex + 1] == '\\')
                {
                    tempString += '\\';
                    tempIndex++;
                }
                tempIndex++;
            }
            return new Token(tempString, startIndex, tempIndex - startIndex + 1);
        }
    }
}