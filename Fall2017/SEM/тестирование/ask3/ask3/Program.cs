﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace task3
{
    public class Program
    {
        static void Main()
        {
            int x = int.Parse(Console.ReadLine());
            string[] tempArr = Console.ReadLine().Split(' ');
            int[] arr = new int[tempArr.Length];
            for (int i = 0; i < tempArr.Length; i++)
                arr[i] = int.Parse(tempArr[i]);
            var result = FindSum(arr, x);
            foreach (var t in result)
                Console.WriteLine(t);
            Console.ReadKey();
        }

        public static int[] FindSum(int[] arr, int x)
        {
            int i = 0, j = arr.Length - 1;
            int first = arr[i], second = arr[j];
            for (int k = 0; k < arr.Length; k++)
            {
                if (first + second > x)
                {
                    j--;
                    second = arr[j];
                }
                else if (first + second < x)
                {
                    i++;
                    first = arr[i];
                }
                else break;
            }
            if (x == first + second)
                return new[] { first, second };
            else return new[] { 0, 0 };
        }
    }
}