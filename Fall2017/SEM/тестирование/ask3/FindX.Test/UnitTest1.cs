﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace task3
{
    [TestClass]
    public class UnitTest1
    {	
	public void TestOnValues(int x, int[] arr, int[] result)
        {
            var findResults = Program.FindSum(arr, x);
            Assert.AreEqual(result, findResults);
        }

        [TestMethod]
        public void FirstTest()
        {
            TestOnValues(32, new[] { 10, 12, 14, 16, 22, 26 }, new[] { 10, 22 });
        }

        [TestMethod]
        public void SecondTest()
        {
            TestOnValues(1, new[] { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 }, new[] { 0, 1 });
        }
    }
}
