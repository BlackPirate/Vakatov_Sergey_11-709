﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Program
{
    static void Main()
    {
        int value = Convert.ToInt32(Console.ReadLine());
        int power = Convert.ToInt32(Console.ReadLine());

        System.Diagnostics.Stopwatch clock = new System.Diagnostics.Stopwatch();

        clock.Start();
        int result = PowerRecurrent(value, power);
        clock.Stop();
        Console.WriteLine(clock.Elapsed.TotalMilliseconds);
        Console.WriteLine(result);

        clock.Restart();
        result = PowerByCycle(value, power);
        clock.Stop();
        Console.WriteLine(clock.Elapsed.TotalMilliseconds);
        Console.WriteLine(result);

        Console.ReadKey();
    }

    static int PowerRecurrent(int value, int power)
    {
        if (power == 0)
        {
            return 1;
        }
        else
        {
            if (power % 2 != 0)
                return PowerRecurrent(value, power - 1) * value;
            else
            {
                int tempResult = PowerRecurrent(value, power / 2);
                return tempResult * tempResult;
            }
        }
    }

    static int PowerByCycle(int value, int power)
    {
        int result = 1;
        while (power > 0)
        {
            if (power % 2 != 0)
            {
                power--;
                result *= value;
            }

            value *= value;
            power /= 2;
        }
        return result;
    }
}