﻿using System;

class Program
{
    static void Main()
    {
        int n = int.Parse(Console.ReadLine());
        int a, b, count = 1;
        int max = 0;
        a = int.Parse(Console.ReadLine());

        for (int i =1; i<n; i++)
        {
            b = int.Parse(Console.ReadLine());
            if (a == b)
            {
                count++;
                if (count > max)
                    max = count;
            }
            else count = 1;

            a = b;
        }

        Console.WriteLine(max);
        Console.ReadKey();
    }
}
