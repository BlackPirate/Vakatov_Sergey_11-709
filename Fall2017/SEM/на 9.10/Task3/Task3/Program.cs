﻿using System;

class Program
{
    static void Main()
    {
        //string ch="";
        int n = Convert.ToInt32(Console.ReadLine());
        //for (int i=1; i <= n; i++)
        //{
        //    ch += i.ToString();
        //}
        //Console.WriteLine(ch[n-1]);

        bool first = true;
        int a=-1;
        if (n < 10)
        {
            a = n;
        }
        else
        {
            if (n % 2 == 0)
            {
                n++;
            }
            else first = false;
            n -= 9;
            int i = n / 2 + 9;
            if (first) a = i / 10;
            else a = i % 10;
        }
        Console.WriteLine(a.ToString());
        Console.ReadKey();
    }
}