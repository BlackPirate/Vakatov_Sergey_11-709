﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class Program
{
    static void Main()
    {
        int n = int.Parse(Console.ReadLine());
        var t = Console.ReadLine().Split(' ');
        int value = int.Parse(Console.ReadLine());
        int[] array = new int[n];
        int[] indexs = new int[n];

        for (int i = 0; i < n; i++)
        {
            array[i] = int.Parse(t[i]);
            indexs[i] = i;
        }

        var sorted = SortArray(array, indexs);
        var finalIndex = FindElement(sorted.Item1, value);

        Console.WriteLine(finalIndex > 0 ? sorted.Item2[finalIndex] : -1);
        Console.ReadKey();
    }

    static Tuple<int[],int[]> SortArray(int[] arr, int[] index)
    {
        int[] array = new int[arr.Length];
        Array.Copy(arr, array, arr.Length);
        for (int i = 0; i < array.Length - 1; i++)
        {
            for (int j = 0; j < array.Length - 1; j++)
            {
                if (array[j] > array[j + 1])
                {
                    Change(ref array[j], ref array[j + 1]);
                    Change(ref index[j], ref index[j + 1]);
                }
            }
        }
        return Tuple.Create<int[], int[]>(array, index);
    }

    static void Change(ref int a, ref int b)
    {
        var t = a;
        a = b;
        b = t;
    }

    static int FindElement(int[] array, int value)
    {
        var left = 0;
        var right = array.Length - 1;
        while (left < right)
        {
            var middle = (right + left) / 2;
            if (value <= array[middle])
                right = middle;
            else left = middle + 1;
        }
        if (array[right] == value)
            return right;
        return -1;
    }
}
