﻿namespace Fraction
{
    public class RacionalFraction
    {
        readonly int numerator;
        readonly uint denominator;

        public RacionalFraction(int numerator, uint denoninator)
        {
            this.numerator = numerator;
            this.denominator = denoninator;

            int div = GetGreatestDivider(numerator, (int)denominator);
            this.numerator /= div;
            this.denominator /= (uint)div;
        }

        public double GetValue() =>
            (double)numerator / denominator;

        public RacionalFraction Sum(RacionalFraction second)
        {
            int resultNumerator = (int)(this.numerator * second.denominator);
            resultNumerator += (int)(second.numerator * this.denominator);
            int resultDenominator = (int)(this.denominator * second.denominator);

            return new RacionalFraction(resultNumerator, (uint)resultDenominator);
        }

        public RacionalFraction Multiply(RacionalFraction second)
        {
            int resultNumerator = this.numerator * second.numerator;
            int resultDenominator = (int)(this.denominator * second.denominator);
            return new RacionalFraction(resultNumerator, (uint)resultDenominator);
        }

        public RacionalFraction Division(RacionalFraction second)
        {
            int tNum = (int)second.denominator;
            int tDen = System.Math.Abs(second.numerator);
            if (second.numerator < 0)
                tNum *= -1;
            return this.Multiply(new RacionalFraction(tNum, (uint)tDen));
        }

        public RacionalFraction Substraction(RacionalFraction second) =>
            this.Sum(new RacionalFraction(-second.numerator, second.denominator));

        public override string ToString() =>
            System.String.Format("{0} / {1}", this.numerator, this.denominator);

        public static RacionalFraction GetFractionFromString(string original)
        {
            var t = original.Split('/');
            if (t.Length == 2)
            {
                int num = int.Parse(t[0]);
                int den = int.Parse(t[1]);
                return new RacionalFraction(num, (uint)den);
            }
            else return new RacionalFraction(1, 1);
        }

        private int GetGreatestDivider(int a, int b)
        {
            while (a * b != 0)
            {
                if (a * a > b * b)
                    a %= b;
                else b %= a;
            }
            return System.Math.Abs(a + b);
        }
    }
}