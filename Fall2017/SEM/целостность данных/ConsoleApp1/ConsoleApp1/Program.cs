﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fraction
{
    class Program
    {
        static void Main(string[] args)
        {
            RacionalFraction a = new RacionalFraction(12, 4);
            RacionalFraction b = new RacionalFraction(-2, 3);
            RacionalFraction t = RacionalFraction.GetFractionFromString(Console.ReadLine());

            Console.WriteLine("T: {0}", t);
            Console.WriteLine("A: {0}", a);
            Console.WriteLine("value A is {0}", a.GetValue());
            Console.WriteLine("B: {0}", b);
            Console.WriteLine("value B is {0}", b.GetValue());
            Console.WriteLine("A * B = {0}", a.Multiply(b));
            Console.WriteLine("A + B = {0}", a.Sum(b));
            Console.WriteLine("A / B = {0}", a.Division(b));
            Console.WriteLine("A - B = {0}", a.Substraction(b));
            Console.WriteLine("A / B + T * A = {0}", a.Division(b).Sum(t.Multiply(a)));
        }
    }
}
