﻿///////////////////////////////////////////////
///            Задание 11 из ulearn         ///
///            Выполнил Вакатов Сергей      ///
///            Группа 11-709                ///
///////////////////////////////////////////////
using System;

class Program
{
    static void Main()
    {
        int h = Convert.ToInt32(Console.ReadLine());
        int m = Convert.ToInt32(Console.ReadLine());

        double ZeroHour = (h % 12) * 30 + m * 0.5;  //сколько проходит часовая стрелка. 30 в час, 0.5 в минуту
        double ZeroMin = 6 * m;                     //6 градусов в минуту

        Console.WriteLine(Math.Abs(ZeroMin - ZeroHour).ToString());
        Console.ReadKey();
    }
}
