﻿///////////////////////////////////////////////
///            Задание 1084 из ulearn       ///
///            Выполнил Вакатов Сергей      ///
///            Группа 11-709                ///
///////////////////////////////////////////////
using System;

class Program
{
    static void Main()
    {
        int lenO = Convert.ToInt32(Console.ReadLine());     //длина стороны огорода
        int lenV = Convert.ToInt32(Console.ReadLine());     //длина веревки

        double s = Math.PI * lenV * lenV;                   //вычисляем площадь круга

        if (lenV > (double)(lenO / 2))
        {
            //если сегмент круга вылезает за пределы квадрата, удаляем его
            double sTriangle = Math.Sqrt(lenV * lenV - (lenO * lenO) / 4) * lenO / 2;
            //вычисляем площадь треугольника, сектора и сегмента
            double sinA = 2*sTriangle / (lenV * lenV);
            double aSin = (Math.Asin(sinA) * 180) / Math.PI;
            double sSec = Math.PI * lenV * lenV * aSin / 360;
            double sSeg = sSec - sTriangle;
            //это квадрат, поэтому таких сегментов 4
            s -= 4 * sSeg;
        }

        s = Math.Round(s, 3);       //достигаем нужной точности

        if (s > lenO * lenO)
        {
            Console.WriteLine((lenO * lenO).ToString());    //если все же площадь фигуры больше, чем площадь огорода
        }
        else Console.WriteLine(s.ToString());

        Console.ReadKey();
    }

    
}
