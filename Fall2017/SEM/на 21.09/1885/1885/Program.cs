﻿///////////////////////////////////////////////
///            Задание 1885 из ulearn       ///
///            Выполнил Вакатов Сергей      ///
///            Группа 11-709                ///
///////////////////////////////////////////////
using System;

class Program
{
    static void Main()
    {
        int h, v, x, t; //высота, макс.скорость, скорость-заложенные-уши, время на подъем

        h = Convert.ToInt32(Console.ReadLine());
        t = Convert.ToInt32(Console.ReadLine());
        v = Convert.ToInt32(Console.ReadLine());
        x = Convert.ToInt32(Console.ReadLine());

        double tMax = (double)h / (x+1);    //с этого момента уши будут заложены
        if (tMax > t) tMax = t;             //но может выйти так, что мы выдйем за пределы времени на подъем
        //в этом случае мы можем найти такую скорость, при которой мы уложимся ровно во время подъема

        double tMin = ((double)h - x * t) / (v - x);
        //решая небольшую систему, приходим к такой формуле для нахождения минимального времени

        Console.WriteLine(tMax.ToString());
        Console.WriteLine(tMin.ToString());

        Console.ReadKey();
    }
}
