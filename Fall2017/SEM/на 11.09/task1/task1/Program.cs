﻿///////////////////////////////////////////////
///            Задание 1 из ulearn          ///
///            Выполнил Вакатов Сергей      ///
///            Группа 11-709                ///
///////////////////////////////////////////////
using System;

class Program
{
    public static void Add(int a, int b) //первый способ
    {
        Console.WriteLine("использован способ 1");

        a += b;     //
        b = a - b;  //самый очевидный альтернативный способ
        a -= b;     //поменять две переменных без использования третьей

        Console.Write(a.ToString() + ' ' + b.ToString());
    }

    public static void Temp(int a, int b) //второй способ
    {
        Console.WriteLine("использован способ 2");

        int t = a;
        a = b;      //дабы значение не потерялось, используем третью переменную
        b = t;

        Console.Write(a.ToString() + ' ' + b.ToString());
    }

    static void Main()
     {
        int a, b;
        Random rnd = new Random(); //выберем способ при помощи этого

        Console.WriteLine("Введите числа, которые следуюет поменять местами");

        a = Convert.ToInt32(Console.ReadLine()); //считываем a, b
        b = Convert.ToInt32(Console.ReadLine());

        if (rnd.Next() % 2 == 0) //определяемся со способом
            Add(a, b);
        else Temp(a, b);

        Console.ReadKey();
     }

    /*
     * принцип работы: 
     * считываем два числа, рандомно определяем один из двух способов поменять их местами.
     * первый: через сумму
     * второй: путем введения третьей переменной
     */
}
