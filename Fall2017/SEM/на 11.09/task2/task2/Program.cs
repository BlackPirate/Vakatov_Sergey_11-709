﻿///////////////////////////////////////////////
///            Задание 2 из ulearn          ///
///            Выполнил Вакатов Сергей      ///
///            Группа 11-709                ///
///////////////////////////////////////////////

using System;


class Program
{
    static void Main()
    {
        int a, b, c=0;

        Console.WriteLine("введите трехзначное число:");

        a = Convert.ToInt32(Console.ReadLine()); //считываем трехзначное число

        while (a > 0)
        {
            c *= 10;    //повышаем на разряд
            b = a % 10; //узнаем последнюю цифру
            a /= 10;    //понижаем оригинальное число на разряд
            c += b;     //добавляем новую цифру
        }

        Console.WriteLine(c.ToString());
        Console.ReadKey();
    }
    /*
     * алгоритм работы: (подходит не толко для трехзначных чисел)
     * в цикле у исходного числа берем по последней цифре, затем понижаем его на разряд.
     * новое числе составляется путем записи полученной в него цифры и повышения на разряд
     */
}

