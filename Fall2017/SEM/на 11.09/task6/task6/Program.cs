﻿///////////////////////////////////////////////
///            Задание 6 из ulearn          ///
///            Выполнил Вакатов Сергей      ///
///            Группа 11-709                ///
///////////////////////////////////////////////

using System;

class Program
{
    static void Main()
    {
        double x1, x2, x3;
        double y1, y2, y3;

        Console.WriteLine("введите координаты первой точки прямой, сначала x, затем y");
        x1 = Convert.ToDouble(Console.ReadLine());
        y1 = Convert.ToDouble(Console.ReadLine());

        Console.WriteLine("введите координаты второй точки прямой, сначала x, затем y");
        x2 = Convert.ToDouble(Console.ReadLine());
        y2 = Convert.ToDouble(Console.ReadLine());

        Console.WriteLine("введите координаты точки, от которой будем искать расстояние");
        x3 = Convert.ToDouble(Console.ReadLine());
        y3 = Convert.ToDouble(Console.ReadLine());

        double lenght = (double)Math.Abs((y1 - y2) * x3 + (x2 - x1) * y3 + (x1 * y2 - x2 * y1)) / (Math.Sqrt((y1 - y2) * (y1 - y2) + (x2 - x1) * (x2 - x1)));
        //формула для нахождения высоты в треугольнике по его вершинам

        Console.WriteLine(lenght.ToString());
        Console.ReadKey();
    }
}
