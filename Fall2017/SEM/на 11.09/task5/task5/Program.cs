﻿///////////////////////////////////////////////
///            Задание 5 из ulearn          ///
///            Выполнил Вакатов Сергей      ///
///            Группа 11-709                ///
///////////////////////////////////////////////

using System;

class Program
{
    static void Main()
    {
        int a, b, count = 0;

        Console.WriteLine("введите границы");

        a = Convert.ToInt32(Console.ReadLine());
        b = Convert.ToInt32(Console.ReadLine());

        count = Year(a, b);

        //однако високосными не считаются года, которые делятся на 100, но не делятся на 4, надо учесть и это

        if (a /100 != b/100) 
        {
            
            count-=(b/100-a/100);
            if (a % 100 == 0)
                count += Year(a / 100, b / 100);
            else count += Year((a / 100)+1, b / 100);
        }

        Console.WriteLine(count.ToString());
        Console.ReadKey();
        
    }

    static int Year(int a, int b)
    {
        if (a == b && a%4!=0) return 0;

        int c = (b - a) + 1;//теперь есть количество лет в отрезке
        c -= b % 4; 
        if (a%4!=0)             //находим ближайший високосный
            c -= 4 - (a % 4);   //находим ближайший с другого конца.
                                /* например, у нас есть года 7 8 9. общее количество: 3.
                                 * до ближайшего високосного по 1 с каждой стороны.
                                 * но если 9%4=1, то 7%4=3, поэтому вычитаем остаток для нижней границы из 4
                                  */
        return c / 4 + 1;       //получаем количество високосных лет.
    }
}