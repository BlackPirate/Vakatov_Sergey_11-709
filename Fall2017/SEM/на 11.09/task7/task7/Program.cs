﻿///////////////////////////////////////////////
///            Задание 7 из ulearn          ///
///            Выполнил Вакатов Сергей      ///
///            Группа 11-709                ///
///////////////////////////////////////////////

using System;

class Program
{
    static void Main()
    {
        Console.WriteLine("Вы желаете задать две точки прямой или написать ее уравнение?\n1:точки;\n2.уравнение");

        int sw = Convert.ToInt32(Console.ReadLine());

        switch (sw)
        {
            case 1:
                Point(); //находим через точки
                break;
            case 2:
                Eq();   //находим через уравнение прямой
                break;
        }

        Console.ReadKey();
    }

    static void Point()
    {
        double x1, x2, y1, y2;

        Console.WriteLine("введите координаты первой точки. сначала x, затем y");
        x1 = Convert.ToDouble(Console.ReadLine());
        y1 = Convert.ToDouble(Console.ReadLine());

        Console.WriteLine("введите координаты второй точки. сначала x, затем y");
        x2 = Convert.ToDouble(Console.ReadLine());
        y2 = Convert.ToDouble(Console.ReadLine());

        Print((x1 - x2), (y1 - y2));
    }
    
    static void Eq()
    {
        string str, k="", b="";
        double x, y;
        bool bePlus = false;

        Console.Write("введите уравнение прямой: y=");  //y=k*x+b. нужны k и b
        str = Console.ReadLine();

        char ch;
        for (int i = 0; i < str.Length; i++)
        {
            ch = str[i];

            if (ch != ' ')
                if (!bePlus)
                {
                    if (ch != 'x' && ch !='*')
                    {
                        if (ch == '+' || ch== '-')
                            bePlus = true;
                        else
                            k += ch;
                    }
                    else continue;
                }
                else
                {
                    b += ch;
                }
        }
        /*
         * про цикл:
         * нам нужны только коэффицент перед x и свободный член, в цикле мы их достем из уравнения.
         */
	if (k == "") k = "0";
        if (b == "") b = "0";
        x = Convert.ToDouble(b);
        y = Convert.ToDouble(k);

        Print(x, y);

    }

    static void Print(double x, double y)
    {
        Console.WriteLine("параллельный: {" + x.ToString() + ',' + y.ToString() + '}');
        Console.WriteLine("перпендикулярный:  {" + ((-1) * y).ToString() + ',' + x.ToString() + '}');
        //скалярное произведение будет равно 0, векторы перпендикулярны
    }
}
