﻿///////////////////////////////////////////////
///            Задание 4 из ulearn          ///
///            Выполнил Вакатов Сергей      ///
///            Группа 11-709                ///
///////////////////////////////////////////////

using System;

class Program
{
    static void Main()
    {
        int x, y, n, count = 0;

        Console.WriteLine("введите N");
        n = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine("введите x,y");
        x = Convert.ToInt32(Console.ReadLine());
        y = Convert.ToInt32(Console.ReadLine());

        count = (n - 1) / x;
        count += (n - 1) / y;
        count -= (n - 1) / (x * y);

        Console.WriteLine(count.ToString());
        Console.ReadKey();

    }
    /*
     * Принцип работы:
     * изначально счетчик равен 0, затем в цикле
     * перебираем все числа от n до 1 и увеличиваем счетчик, когда находим такое число,
     * которое делится на x ИЛИ y
     */
}
