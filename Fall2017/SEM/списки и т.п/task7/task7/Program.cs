﻿using System;

class Program
{
    static void Main()
    {
        int n = int.Parse(Console.ReadLine());
        string[] temp = Console.ReadLine().Split(' ');
        int[] arr = new int[n];

        for (int i = 0; i < n; i++)
            arr[i] = int.Parse(temp[i]);

        Array.Sort(arr);

        Console.WriteLine(arr[n / 2]);
        Console.ReadKey();
    }
}