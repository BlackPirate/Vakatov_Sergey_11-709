﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace EmptyWithAutorization.Models
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "А рассылку куда присылать?")]
        public string Email { get; set; }

        [Required(ErrorMessage = "А пароль за вас Пушкин придумает?")]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Запомните уже ваш пароль!")]
        public string ConfirmPassword { get; set; }
    }
}
