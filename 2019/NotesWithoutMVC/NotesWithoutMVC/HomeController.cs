﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Routing;
using System;
using System.Threading.Tasks;

namespace NotesWithoutMVC
{
    public class HomeController
    {
        private RouteContext context;

        public HomeController(RouteContext context)
        {
            this.context = context;
        }

        public void ErrorPage()
        {
            context.Handler = async ctx =>
            {
                ctx.Response.ContentType = "text/html;charset=utf-8";
                await ctx.Response.WriteAsync("Errororororor");
            };
        }

        public void HomePage()
        {
            context.Handler = async ctx =>
            {
                ctx.Response.ContentType = "text/html;charset=utf-8";
                await ctx.Response.WriteAsync("Home");
            };
        }

        public void Index()
        {
            context.Handler = async ctx =>
            {
                ctx.Response.ContentType = "text/html;charset=utf-8";
                await ctx.Response.WriteAsync("Index");
            };
        }
    }
}
