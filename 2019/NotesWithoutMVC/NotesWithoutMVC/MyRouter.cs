﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Routing;
using System;
using System.Threading.Tasks;
using System.Reflection;

namespace NotesWithoutMVC
{
    public class MyRouter : IRouter
    {
        HomeController controller;

        public VirtualPathData GetVirtualPath(VirtualPathContext context)
        {
            throw new NotImplementedException();
        }

        public Task RouteAsync(RouteContext context)
        {
            controller = new HomeController(context);

            if (context.RouteData.Values["controller"]?.ToString() == controller.GetType().Name)
            {
                var action = context.RouteData.Values["action"];
                try
                {
                    var metod = controller.GetType().GetMethod(action.ToString());
                    metod.Invoke(controller, null);
                }
                catch
                {
                    controller.ErrorPage();
                }
            }
            else controller.ErrorPage();
            return Task.CompletedTask;
        }
    }
}
