﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using Microsoft.AspNetCore.Mvc;

namespace WebNotes.Controllers
{
    public class NoteStorageController : Controller
    {
        int id = -1;

        public IActionResult Index()
        {
            if (id < 0)
            {
                var files = GetFiles();
                id = 1;
                foreach (var file in files)
                {
                    string name, text;
                    using (var reader = new StreamReader(file))
                    {
                        name = reader.ReadLine();
                        text = reader.ReadLine();
                    }
                    System.IO.File.Delete(file);
                    SaveNote(name, text);
                }
            }
            return View();
        }

        public IActionResult SaveNote(string name, string text)
        {
            var note = System.IO.File.Create($"notes/{id++}");

            using (TextWriter textWriter = new StreamWriter(note))
            {
                name = name == "" ? "emptyName" : name;
                text = text == "" ? "emptyNote" : text;
                textWriter.WriteLine(name);
                textWriter.WriteLine(text);
                ViewData["Name"] = name;
                ViewData["Text"] = text;
            }
            return View();
        }

        public IActionResult GetList()
        {
            ViewBag.AllNotes = System.IO.Directory.GetFiles("notes");
            return View();
        }

        public FileResult DownloadNote(string path)
        {
            byte[] mas = System.IO.File.ReadAllBytes(path);
            string file_type = "application/txt";
            string file_name = "note";
            return File(mas, file_type, file_name);
        }

        private string[] GetFiles()
        {
            var files = System.IO.Directory.GetFiles("notes");
            return files;
        }

        public IActionResult NotePreview(string path)
        {
            using (TextReader reader = new StreamReader(path))
            {
                ViewData["Name"] = reader.ReadLine();
                ViewData["Text"] = reader.ReadLine();
                ViewData["Path"] = path;
            }
            return View();
        }

        public IActionResult ChangeNote(string path)
        {
            using (TextReader reader = new StreamReader(path))
            {
                ViewData["Name"] = reader.ReadLine();
                ViewData["Text"] = reader.ReadLine();
            }
            return View();
        }

        public IActionResult DeleteNote(string path)
        {
            System.IO.File.Delete(path);
            return Index();
        }
    }
}