﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using WebNotes.Models;

namespace WebNotes.Controllers
{
    public class NotesController : Controller
    {
        private readonly WebNotesContext _context;
        static Dictionary<int, string> UserIdToSessionId = new Dictionary<int, string>();

        public NotesController(WebNotesContext context)
        {
            _context = context;
        }

        // GET: Notes
        public async Task<IActionResult> Index()
        {
            //Response.Cookies.Delete("user");
            //return View(await _context.Note.Where(note => note.Id == userId).ToListAsync());
            if (!IsAutorized())
                return RedirectToAction(nameof(Autorization));

            //var id = HttpContext.Session.Id;

            return View(await _context.Note.ToListAsync());
        }

        // GET: Notes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var note = await _context.Note
                .FirstOrDefaultAsync(m => m.Id == id);
            if (note == null)
            {
                return NotFound();
            }

            return View(note);
        }

        // GET: Notes/Create
        public IActionResult Create()
        {
            if (!IsAutorized())
                return RedirectToAction(nameof(AutorizationError));
            return View();
        }

        public IActionResult Registration([Bind("Login,Password,Id")] User user)
        {
            if (_context.User.Where(x => x.Login == user.Login).FirstOrDefault() != null)
            {
                if (user.Login != null && user.Password != null)
                {
                    _context.User.Add(user);
                    _context.SaveChanges();
                    return RedirectToAction(nameof(Index));
                }
            }
            return View();
        }

        private bool IsAutorized()
        {
            var userId = Request.Cookies["user"];
            if (userId == null || userId == "")
                return false;
            if (UserIdToSessionId.ContainsKey(int.Parse(userId)))
                return UserIdToSessionId[int.Parse(userId)] == HttpContext.Session.Id;
            else return false;
        }

        public IActionResult AutorizationError()
        {
            return View();
        }

        public IActionResult LogOut()
        {
            Response.Cookies.Delete("user");    
            return RedirectToAction(nameof(Autorization));
        }

        public IActionResult Autorization(string login, string password)
        {
            var user = _context.User.Where(u => u.Login == login).FirstOrDefault();
            if (user != null && user.Password == password)
            {
                Response.HttpContext.Session.Set(user.Login, new[] { (byte)(user.Id) });
                Response.Cookies.Append("user", user.Id.ToString());
                UserIdToSessionId[user.Id] = HttpContext.Session.Id;
                return RedirectToAction("Index");
            }
            return View();
        }


        // POST: Notes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Name,Text,Id")] Note note)
        {
            if (ModelState.IsValid)
            {
                _context.Add(note);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(note);
        }

        // GET: Notes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var note = await _context.Note.FindAsync(id);
            if (note == null)
            {
                return NotFound();
            }
            return View(note);
        }

        // POST: Notes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Name,Text,Id")] Note note)
        {
            if (id != note.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(note);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!NoteExists(note.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(note);
        }

        // GET: Notes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var note = await _context.Note
                .FirstOrDefaultAsync(m => m.Id == id);
            if (note == null)
            {
                return NotFound();
            }

            return View(note);
        }

        // POST: Notes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var note = await _context.Note.FindAsync(id);
            _context.Note.Remove(note);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool NoteExists(int id)
        {
            return _context.Note.Any(e => e.Id == id);
        }
    }
}
