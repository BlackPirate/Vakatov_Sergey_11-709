﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace WebNotes.Models
{
    public class WebNotesContext : DbContext
    {
        public WebNotesContext (DbContextOptions<WebNotesContext> options)
            : base(options)
        {
        }

        public DbSet<WebNotes.Models.Note> Note { get; set; }
        public DbSet<User> User { get; set; }
    }
}
